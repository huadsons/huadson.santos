import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { series } = this.props
        return (
            <React.Fragment>
                <h2>{ series.titulo }</h2>
                <h2>{ series.anoEstreia }</h2>
                <h2>{ series.diretor }</h2>
                <h2>{ series.distribuidora }</h2>
                <h2>{ series.elenco }</h2>
                <h2>{ series.genero }</h2>
                <h2>{ series.generoEpisodios }</h2>
                <h2>{ series.temporadas }</h2>
            </React.Fragment>
        )
    }
}