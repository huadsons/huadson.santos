/* 
Coloque um campo textual na tela onde o usuário poderá digitar o número do pokémon a ser consultado. 
Caso o usuário tenha digitado um número, dispare a busca do pokémon daquele número.
Caso contrário, exiba um erro na tela “Digite um id válido”. A busca e exibição dos dados devem ser feitas quando o 
campo perder o foco (exemplo: tab ou clicar fora do campo).
*/

let num = document.getElementById('numero')

num.addEventListener('blur', () => {
    let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);

    pokemon.then(() => { 
        if ( !isNaN(num.value) ) {
    
            console.log(num.value)
            
            function validacao(e){
                let target = e.target;
                let msg = '';
                switch (target.id) {
                    case "numero":
                        if(target.value == isNaN(num)){
                            msg = "Esse campo não pode ser vazio";
                        }else{
                            msg = '';
                        }  
                        break;
                    case "email":
                        break;
                    default:
                        break;
                }
                let erro = document.getElementById(`msg-erro-${target.name}`)
                erro.innerHTML = msg;
            }
            
            //let num = document.getElementById('numero');
            //for (let i = 0; i < num.length; i) {
                
            //}
                //resolve('https://pokeapi.co/api/v2/pokemon/');
            
        } else {
            console.log("nao é um numero")
        }
    })
    .catch( data => console.log("erro"));
});


/* pokemon
    .then( data => data.json() )
    .then( data => console.log(data.results) ); */