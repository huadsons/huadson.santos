/* let vemSer = {
    local: 'DBC',
    ano: '2020',
    imprimirInformacoes: function(quantidade, modulo) {
        return `Argumentos: ${ this.local } | ${ this.ano } | ${ quantidade } | ${ modulo }`;
    }
}

console.log(vemSer.imprimirInformacoes(20, 3)); */



class Jedi {
    constructor(nome){ //dentro da classe não precisa colocar function pois ele entende como função
        this.nome = nome;
    }

    atacarComSabre(){
        setTimeout(() => {
            console.log(`${ this.nome } atacou com sabre:`)//quando não quero que a função seje executada exatamente quando for chamada
        }, 1000); // tempo em milisegundos para a função ser executada

    }

    atacarComSabre(){
        let self = this //escropo local usando o this do documento 
        setTimeout(function() {
            console.log(`${ self.nome } atacou com sabre:`)//quando não quero que a função seje executada exatamente quando for chamada
        }, 2000);
    }

}

// let luke = new Jedi("Luke");

// luke.atacarComSabre();

// console.log(luke);

/* let defer = new Promise((resolve, reject) => { 
    setTimeout(() => {
        if(true){
            resolve('Foi Resolvido');   //quando a promessa for resolvida
        }else{
            reject('Erro'); //quando a promessa não for resolvida será tratada dentro do reject
        }
    }, 2000);          
});

defer
    .then((data) => {   // trata o resultado de uma promessa
        console.log(data)
        return "Novo Resultado" //then encadeado
    })
    .then((data) => console.log(data)) 
    .catch((erro) => console.log(erro))//tratar o erro da promessa

    //status de uma promessa pode ser: pendente , resolvida ou rejeitada */

    let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);

    pokemon
        .then( data => data.json() )
        .then( data => console.log(data.results) );