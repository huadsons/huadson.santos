//console.log("Cheguei Aqui"); usado aqui separado porque se der manutenção só faz nesse arquivo



var nomeDaVariavel = "valor";
let = nomeDaLet = "ValorLet"; //no mesmo escopo o let só declara uma vez e ele sempre é local.
const nomeDaConst = "ValorConst"; // só declar uma vez e só inicializa uma vez também.

var nomeDaVariavel = "Valor3";
nomeDaLet = "ValorDaLet2";

const nomeDaConst2 = {
    nome:"Huadson",
    idade: 32
}; //Quando se usa chaves é um objeto.

Object.freeze(nomeDaConst2); //para não poder mudar vc congela 

nomeDaConst2.nome = "Huadson Santos";//ao colocar ponto(.) acessa o valor das constantes e pode mudar

//console.log(nomeDaConst2);

function nomeDaFuncao() {
    
    nomeDaVariavel = "valor";
    let nomeDaLet = "ValorLet2";
}

//console.log(nomeDaLet);


//Funções pode ser recebendo ou não atributos


function somar() {

}

function somar(valor1, valor2 = 1) { //não precisa atribuir tipo de variavel // se passar somente o valor1 o valor2 por definição fica com = 1
    console.log(valor1 + valor2);
}

somar(3);
somar(3, 2);

/* console.log(1 + 1);
console.log(1 + "1");*/

function ondeMoro(cidade){
    console.log("Eu moro em " + cidade);
    console.log(`Eu moro em ${cidade}. E sou feliz ${ 30 + 1} dias`);
}

//ondeMoro("Canoas");

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pessego"
                + "\n";

                //quando se coloca crase ele reconhece como o dev digita na tela respeitando todos espaços, quebra de linha
    let newTexto = ` 
                Banana
                    Ameixa
                        Goiaba
                            Pessego
                `;
                console.log(texto);
                console.log(newTexto)
}

fruteira();

let eu = {
    nome: "Huadson",
    idade: 32,
    altura: 1.90
};

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} e ${pessoa.altura} de altura`);
}

//quemSou(eu);

let funcaoSomarValores = function(a, b){
    return a + b;
}

let add = funcaoSomarValores
let resultado = add(3, 5)

/* console.log(resultado); */

//forma facil de atribuir valores a um parametro ou pegar um objeto e destrui-lo

const { nome:n, idade:i} = eu // destruction é: pegando o objeto eu e destruindo e definindo com valores
                            // o dois pontos n (:n) muda a constante de nome para n
/* console.log(nome, idade);
console.log(n, i); */


const array = [1, 3, 4, 8];
const [n1, , n3, n2, n4 = 18] = array; // o nome dado vincula a posição do array, se quiser pular coloca entre virgula vazia

/*console.log(n1, n2, n3, n4);*/
function testarPessoa({ nome, idade, altura}) {
    console.log(nome, idade, altura);
}

//testarPessoa(eu);


//trocar valores

let aux = a1;
a1 = b1;
b1 = aux;


let a1 = 1;

let b1 = 2;

let c1 = 3;

let d1 = 4;

/* console.log(a1, b1, c1, d1); */

[a1, b1, c1, d1] = [b1, d1, a1, c1]; //troca atraves do destruction os valores de a1 e b1

/* console.log(a1, b1, c1, d1); */