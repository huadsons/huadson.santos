import React from 'react';

export default ( props ) =>

//<React.Fragment só utilizado para não usar div:

<React.Fragment>
    { props.nome } { props.sobrenome }

</React.Fragment>




/* [ //criar componentes de uma família

    //devido toda vez que for chamado ele vai criando divi, crescendo assim o código deve usar melhor assim:

    props.nome,
    props.sobrenome
] */

// eslint-disable-next-line no-lone-blocks
{/* <div>
        Aqui
        { props.nome } { props.sobrenome }
    </div> */}