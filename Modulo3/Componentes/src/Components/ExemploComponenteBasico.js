import React from 'react';

const CompA = props => <h1>Primeiro</h1> //Criação de componente mais básico em constante
const CompB = () => <h1>Segundo</h1> //Criação de componente mais básico

export default CompA    // fazer aa utilização destes componentes no App.js
export { CompB }        //só pode exportar com um default e os outro vai dentro de chaves