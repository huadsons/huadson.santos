import React, { Component } from 'react';
import './App.css';

import CompA, { CompB } from './Components/ExemploComponenteBasico'//importando os componentes feito no ExemploComponenteBasico.js
import Membros from './Components/Membros.jsx'

class App extends Component { //dentro da class tem as funçoes e o render é coisas que vai na tela
  /* constructor( props ) {
    super( props );
  } */

  render() {
    return (
      <div className="App">
        <Membros />
        
        <Membros nome="João" sobrenome="Silva" /> {/* passando propriedades */}
        <Membros nome="Maria" sobrenome="Silva" />
        <Membros nome="Pedro" sobrenome="Silva" />

        <CompA />
        <CompA />
        <CompB />
        <CompA />
        <CompA />
        <CompA />

      </div>
    );
  }
}

export default App;
