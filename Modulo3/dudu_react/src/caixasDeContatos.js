import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import './layout.css';
import img_1 from './Conteudo_4/img/Linhas-de-Serviço-Agil.png'
import img_2 from './Conteudo_4/img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import img_3 from './Conteudo_4/img/Linhas-de-Serviço-DBC_Software-Builder.png'
import img_4 from './Conteudo_4/img/Linhas-de-Serviço-DBC_Sustain.png'

export default class Caixas extends Component {
  render() {
      return (
        <React.Fragment>
            <div class="row">
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <div>
                            <img src={ img_1 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <Link className="button button-blue" to="/">Home</Link>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <div>
                            <img src={img_2}></img>
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <div>
                            <img src={ img_3 }></img>
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a className="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <div>
                            <img src={img_4}></img>
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
            </div>
        </React.Fragment>
      )
  }
}
