import React, { Component } from 'react';
import './App.css';
import './layout.css';

import Header from './header';
import Banner from './banner';
import Caixas from './caixas_de_contatos';
import Footer from './footer';


//import Contato from '../src/contato.html'; //pagina qued vai ser importada

export default class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <Caixas />
        <Footer />
      </div>
    );
  }
}
