import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './App.css';
import './layout.css';
import img1 from './Conteudo_4/img/Linhas-de-Serviço-Agil.png';
import img2 from './Conteudo_4/img/Linhas-de-Serviço-DBC_Smartsourcing.png';
import img3 from './Conteudo_4/img/Linhas-de-Serviço-DBC_Software-Builder.png';
import Header from './header';
import Banner from './banner';
import Footer from './footer';

export default class Servicos extends Component {

  render() {
      //const{ episodio } = this.props
      return (
        <React.Fragment>
            <Header />
            <Banner />

            <section>
            <div class="row">
                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img1 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img2 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>

                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img3 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
            
        
                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img1 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>

                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img2 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>

                <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={ img3 } />
                        </div>                    
                        <h3>Título</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quis dolor sequi vel. Corporis, totam? Cumque vero inventore magnam natus, expedita consequuntur, voluptatem eaque neque excepturi deleniti molestiae, ullam accusamus!
                        </p>
                        <a class="button button-blue" href="#">Saiba Mais</a>
                    </article>
                </div>
            </div>
        </section>

            <Footer />
        </React.Fragment>
      )
  }
}
