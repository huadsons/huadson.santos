import React, { Component } from 'react';
import ProtoTypes from 'prop-types'

export default class MeuInputNumero extends Component {

    perderFoco = evt => {
        const { obrigatorio, atualizarValor } = this.props
        const nota = evt.target.value
        const erro = obrigatorio && !nota
        atualizarValor( { nota, erro } )
    }

    render() {
        const { placeholder, visivel, mensagemCampo, deveExibirErro } = this.props

        return visivel ? (
            <React.Fragment>
                <div className="Main-header">
                    {
                        mensagemCampo && <span> { mensagemCampo } </span>
                    }
                    {
                        <input type="number" className={ deveExibirErro ? 'erro' : '' } onBlur={ this.perderFoco } placeholder={ placeholder } />
                    }
                    {
                        deveExibirErro && <span className="mensagem-erro"> Obrigatório </span>
                    }
                </div>
            </React.Fragment>
        ): null
    }
}

MeuInputNumero.ProtoTypes = {
    visivel: ProtoTypes.bool.isRequired,
    deveExibirErro: ProtoTypes.bool.isRequired,
    placeholder: ProtoTypes.string,
    mensagemCampo: ProtoTypes.string,
    obrigatorio: ProtoTypes.bool
}

MeuInputNumero.defaultProps = {
    obrigatorio: false
}