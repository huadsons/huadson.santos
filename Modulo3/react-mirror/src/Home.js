import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./layout.css"

export default class Home extends Component {
    render() {
        
        return (
          <div className="App-header">
            <Link className="button btn-green" to="/Jsflix">JSFlix</Link>           
            <Link className="button btn-blue" to="/mirror">React - Mirror</Link>
          </div>
        );
      }
}
