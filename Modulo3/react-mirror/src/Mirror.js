import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './components/episodioUi';
import MeuInputNumero from './components/MeuInputNumero'
import MensagemFlash from './components/mensagemFlash';
//import Card from './components/card';

class Home extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota( { nota, erro } ){
    this.setState({
      deveExibirErro: erro
    })
    if (erro){
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
      
    }else{
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  /* geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) } ></input>
            </div>
          )
        }
      </div>
    )
  } */

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this

    return (
      <div className="App">
        { /*  deveExibirMensagem ? ( <span>Registramos sua nota!</span> ) : ''  */ }
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ exibirMensagem } 
                        mensagem={ mensagem }
                        cor={ cor }
                        segundos={ 5 } />
         <header className='App-header'>
         <div>
            <Link className="button" to="/">Home</Link>
            <Link className="button btn-green" to="/Jsflix">JSFlix</Link>           
          </div>
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
            <button className="btn vermelho">
              {/* <Link to="./avaliacoes"></Link> */}
              <Link to={{ pathname:"/avaliacoes", state: { listaEpisodios } }}>Avaliacao</Link> 
            </button>

          </div>
          { /* this.geraCampoDeNota() */ }
          <MeuInputNumero placeholder="1 a 5"
                          mensagemCampo="Qual sua nota para esse episódio?"
                          visivel={ episodio.assistido || false }
                          obrigatorio={ true }
                          atualizarValor={ this.registrarNota.bind( this ) }
                          deveExibirErro={ deveExibirErro }/>
        </header>
      </div>
    );
  }
}

export default Home;
