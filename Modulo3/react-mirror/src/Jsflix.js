import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import ListaSeries from './models/ListaSeries';
import './Home'
import './Mirror'

export default class App  extends Component{
  constructor(props){
    super(props)
    this.listaSeries = new ListaSeries();
    /*console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.filtrarPorAno(2020))
    console.log( this.listaSeries.procurarPorNome('Winona Ryder') );
    console.log( this.listaSeries.mediaDeEpisodios() );
    console.log( this.listaSeries.totalSalarios(2) );
    console.log( this.listaSeries.queroGenero( 'Caos' ) );
    console.log( this.listaSeries.queroTitulo( 'The' ) );
    console.log( this.listaSeries.creditos( 0 ) );*/
    
  }

  filtrarPorAno(ano){
    return this.todos.filter( serie => {
        return serie.anoEstreia >= ano
    })
  }

  procurarPorNome(nome){
      let encontrou = false;
      this.todos.forEach( serie => {
          let achou = serie.elenco.find( nomeElenco => nome === nomeElenco )
          if(achou){
              encontrou = true
          }
      })
      return encontrou
  }

  mediaDeEpisodios() {
      return parseFloat( this.todos.map( serie => serie.numeroEpisodios ).reduce( (prev, cur) => prev + cur, 0 ) / this.todos.length )
  }

  totalSalarios( indice ){
      function imprimirBRLOneLine( valor ) {
          return parseFloat( valor.toFixed( 2 ) ).toLocaleString('pt-BR', {
              style: 'currency',
              currency: 'BRL'
          })
      }
      const salarioDirecao = 100000 * this.todos[ indice ].diretor.length
      const salarioAtores = 40000 * this.todos[ indice ].elenco.length
      return imprimirBRLOneLine( parseFloat( salarioAtores + salarioDirecao ) )
  }

  queroGenero( genero ){
      return this.todos.filter( serie => {
          return Boolean( serie.genero.find( g => g === genero ) )
      } )
  }

  queroTitulo( titulo ){
      let palavrasTitulo = titulo.split(' ')
      return this.todos.filter( serie => {
          let palavrasSerie = serie.titulo.split(' ')
          return Boolean( palavrasSerie.find( palavra => palavra.includes( palavrasTitulo ) ) )
      } )
  }

  creditos( indice ){
      this.todos[ indice ].elenco.sort( (a,b) => {
          let ArrayA= a.split(' ')
          let ArrayB= b.split(' ')  
          return ArrayA[ ArrayA.length - 1 ].localeCompare( ArrayB[ArrayB.length - 1] )          
      })
      this.todos[ indice ].diretor.sort( (a,b) => {
          let ArrayA= a.split(' ')
          let ArrayB= b.split(' ') 
          return ArrayA[ ArrayA.length - 1 ].localeCompare( ArrayB[ArrayB.length - 1] )                     
      })
      let arrayParaJuntar = []
      arrayParaJuntar.push(`${this.todos[ indice ].titulo}`)
      arrayParaJuntar.push(`Diretores`)  
      this.todos[ indice ].diretor.forEach( d =>{
          arrayParaJuntar.push(`${ d }`)
      })
      arrayParaJuntar.push(`Elenco`)
      this.todos[ indice ].elenco.forEach( e =>{
          arrayParaJuntar.push(`${ e }`)
      })
      return arrayParaJuntar;
  }

  render(){

    return(
        <div className="App">
        <header className="App-header">
            <div>
                <Link className="button" to="/">Home</Link>          
                <Link className="button btn-blue" to="/mirror">React - Mirror</Link>
            </div>
            <span>Por qual ano deseja filtrar?</span>
            <input className="col-2" type="number" placeholder="Digite o ano" onBlur={ this.filtrarPorAno.bind( this ) } ></input>
            <span>Digite o nome do ator que deseja encontrar</span>
            <input className="col-2 " type="number" placeholder="Nome" onBlur={ this.procurarPorNome.bind( this ) } ></input>
            <button className="button-blue" onClick={ this.mediaDeEpisodios.bind( this ) }>Média de eposódios</button>
            <button className="button-green" onClick={ this.totalSalarios.bind( this ) }>Total de salários</button>
            <span>Digite o gênero que deseja encontrar</span>
            <input className="col-2" type="number" placeholder="Nome" onBlur={ this.queroGenero.bind( this ) } ></input>
            <span> { this.queroGenero } </span>
            <span>Digite o Título que deseja encontrar</span>
            <input className="col-2" type="number" placeholder="Nome" onBlur={ this.queroTitulo.bind( this ) } ></input>
            <button className="button-blue" onClick={ this.creditos.bind( this ) }>creditos</button>
        </header>
      </div>
    )
  }
}
