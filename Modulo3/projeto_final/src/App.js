import React,{ Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Home from './Home'
import Listagem from './Components/Listagem'
import Agencias from '../src/Components/Agencias'
import Clientes from './Components/Clientes'
import ContaDeClientes from './Components/ContaDeClientes'
import TipoDeContas from './Components/TipoDeContas';

export default class App extends Component {
    constructor(props){
      super(props)
      this.state ={
        user:[]
      };
    }
     validar(){
      const user = localStorage.getItem('user-autorization')
      if(user){
        this.setState({user: JSON.parse(user)})
      }
      console.log(user)
    } 

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={ Home } />
          <Route path="/listagem" component={ Listagem } />
          <Route path="/agencias" component={ Agencias } />
          <Route path="/clientes" component={ Clientes } />
          <Route path="/contadeclientes" component={ ContaDeClientes } />
          <Route path="/tipodecontas" component={ TipoDeContas } />
        </Switch>
      </Router>
    );
  }
}
