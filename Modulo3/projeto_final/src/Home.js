import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import axios from 'axios';
import { Redirect } from 'react-router-dom'

    class Home extends PureComponent {
        constructor(props) {
          super(props)
      
          this.state = {
            email: '',
            senha: '',
            redirecionaParaProximaPagina: false,
          }
        }
      
        handleChange = (event) => {
            if (event.target.name === "senha"){
                this.setState({ senha: event.target.value })
            }
      
            if (event.target.name === "email"){
                this.setState({ email: event.target.value })
            }
        }
      
        onClickEntrar = async () => {
            const { email, senha, redirecionaParaProximaPagina }= this.state
            const response = await axios.post('http://localhost:1337/login',{
                email: this.state.email, senha: this.state.senha}).then(response =>{
                        if (response.status === 200) {
                            localStorage.setItem('user-autorization', response.data.token)
                            this.setState({ redirecionaParaProximaPagina: true })
                                if (response.status === 400) {
                                    this.setState({ redirecionaParaProximaPagina: false })
                                } 
                        }
            
                })
        }

        render(){

            const { redirecionaParaProximaPagina } = this.state
        
            if (redirecionaParaProximaPagina){
                return (<Redirect to={{pathname: '/Listagem'}} /> )
            }
            else
                return (
                    <React.Fragment>
                <div className="App-header">
                    <div className="d-flex justify-content-center">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input name={ 'email' } type={"text"} funcao={this.handleChange} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input name={'senha'} type={"password"} funcao={this.handleChange} class="form-control" id="exampleInputPassword1" />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        
                        <Link to ={{ pathname: "/listagem" }}><button funcao={this.onClickEntrar} type="submit" class="btn btn-primary">Submit</button></Link>

                    </div>
                </div>
            </React.Fragment>
            );
        }

}
export default Home;

