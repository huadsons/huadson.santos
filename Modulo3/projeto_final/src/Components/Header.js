import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        return (
            <React.Fragment>
                <div className="App-header">
                    <div className="d-flex justify-content-center">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center">
                        
                        <Link to ={{ pathname: "/listagem" }}><button type="submit" class="btn btn-primary">Submit</button></Link>

                    </div>
                </div>
            </React.Fragment>
        );
    }
}
