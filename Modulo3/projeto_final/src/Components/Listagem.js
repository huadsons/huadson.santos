import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

export default class Listagem extends PureComponent {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        return (
            <React.Fragment>
                <div className="App-header">
                    <div class="col col-2 card text-white bg-primary mb-3 ">
                        <Link to ={{ pathname: "/agencias" }}><button type="button" class="btn btn-primary">Agencias</button></Link>
                    </div>
                    <div class="col col-2 card text-white bg-primary mb-3 ">
                        <Link to ={{ pathname: "/clientes" }}><button type="button" class="btn btn-primary">Clientes</button></Link>
                    </div>
                    <div class="col col-2 card text-white bg-primary mb-3 ">
                        <Link to ={{ pathname: "/contadeclientes" }}><button type="button" class="btn btn-primary">Conta de Clientes</button></Link>
                    </div>
                    <div class="col col-2 card text-white bg-primary mb-3 ">
                        <Link to ={{ pathname: "/tipodecontas" }}><button type="button" class="btn btn-primary">Tipo de Contas</button></Link>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
