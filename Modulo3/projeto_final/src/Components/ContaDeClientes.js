import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class ContaDeClientes extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        return (
            <React.Fragment>
                <Link to ={{ pathname: "/listagem" }}><button type="submit" class="btn navbar navbar-brand navbar-expand-lg navbar-light bg-light">Listagem</button></Link>
                 <div className="App-agencias">
                    <ul class="list-group card bg-primary mb-3">
                        <li class="list-group-item active">Conta de Clientes</li>
                        <li class="list-group-item">Conta de cliente 01</li>
                        <li class="list-group-item">Conta de cliente 02</li>
                    </ul>
                </div>
            </React.Fragment>
        );
    }
}
