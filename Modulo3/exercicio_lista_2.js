let moedas = ( function(){
    //Tudo é privado
    function imprimirGBP( params ){
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil( numero * fator ) / fator;
        }
    

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params

        let qtdCasasMilhares = 3
        let stringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero)%1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length

        let c =1
        while(parteInteiraString.length > 0) {
            if(c % qtdCasasMilhares == 0){
                stringBuffer.push( `${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c )}`) //o slice serve para pzssar qual a quantidade eu quero ter
                parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
            }else if(parteInteiraString.length < qtdCasasMilhares){
                stringBuffer.push( parteInteiraString )
                parteInteiraString = ''
            }
            c++
        }

        stringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace( '0,', '').padStart(2,0)
        numeroFormatado = `${ stringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado))

       //return stringBuffer

    }

    //Tudo é público, ou seja , tudo que estiver dentro de return é publico para acesso
    return {
        imprimirBRL: (numero) => 
            imprimirGBP({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`
            })
        
    }
})()

alert(moedas.imprimirBRL(0));
alert(moedas.imprimirBRL(3498.99));
alert(moedas.imprimirBRL(-3498.99));
alert(moedas.imprimirBRL(2313477.0135));