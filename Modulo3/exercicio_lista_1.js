/*Exercício 01 - Crie uma função "calcularCirculo" que receba um objeto com os seguintes
parâmetros: {
    		raio, // raio da circunferência
            tipoCalculo // se for "A" cálcula a área, se for "C" calcula a
            circunferência
	        }



    radius = prompt("Digite um raio: ");
    tipoCalculo = prompt("Digite A para calculo de área ou C para Circunferência: ");



function calculaCirculo(radius, tipoCalculo){

    switch (tipoCalculo){
        case 'A':
            aux = Math.ceil(Math.PI * Math.pow(radius, 2)); // o Math.ceil arredonda pra cima no inteiro
            break;
        case 'C':
            aux = Math.PI * 2 * radius;
            break;
        default:

    }
    console.log(aux);
}

calculaCirculo(radius, tipoCalculo);*/


// Exercício 02 - Crie uma função naoBissexto que recebe um ano (número) e verifica se
// ele não é bissexto. Exemplo:

// 	naoBissexto(2016) // false
// 	naoBissexto(2017) // true

//     ano = prompt("Digite um ano:");

// function naoBissexto(ano) {

//     if(ano % 4 == 0 || ano % 100 != 0 && ano % 400 == 0){
//         console.log("É bissexto");
//     }

//     else {
//         console.log("Não é Bissexto");
//     }
// }

// naoBissexto(ano);

// ou 

// let bissexto = ano => (ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0) ? true : false; // sendo uma linha só não precisa usar chaves, é uma função, se estiver mais de um escopo endenta normalmente

/*Exercício 03 - Crie uma função somarPares que recebe um array de números (não precisa
fazer validação) e soma todos números nas posições pares do array, exemplo:

    somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34 */


// function somarPares(numeros) {
//     let resultado = 0;
//     for (let i = 0; i < numeros.length; i+=2) {
//         resultado += numeros[i];
//     }
//     return resultado;
// }



/* Exercício 04 - Escreva uma função adicionar que permite somar dois números através de duas chamadas
diferentes (não necessariamente pra mesma função). Pirou? Ex:

adicionar(3)(4) // 7
adicionar(5642)(8749) // 14391  */



/* function somar1(valor1){
    function somar2(valor2){
        return valor1 + valor2;
    }
    return somar2;
}

console.log(somar1(91)(8));

ou

let adicionar = op1 => op2 => op1 + op2;

const is_divisivel = (divisor, numero) => !(numero % divisor); // saber se o valor é divisível
const divisor = 2; // para saber se vários divisores são divisiveis por 2
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12));


const is_divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(20));
console.log(is_divisivel(17));
console.log(is_divisivel(22));*/


/* Exercício 05 - Escreva uma função imprimirBRL que recebe um número flutuante
(ex: 4.651) e retorne como saída uma string no seguinte formato (seguindo o exemplo):
“R$ 4,66”

Outros exemplos:

imprimirBRL(0) // “R$ 0,00”
imprimirBRL(3498.99) // “R$ 3.498,99”
imprimirBRL(-3498.99) // “-R$ 3.498,99”
imprimirBRL(2313477.0135) // “R$ 2.313.477,02” 

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, no Brasil, é a vírgula
OBS: o separador milhar, no Brasil, é o ponto

function imprimirBRL(valor){
    var valorArredondado = Math.abs(valor.toFixed(2));
    return console.log(`${valor > 0 ? '' : '-' }R$ ${valorArredondado}`);
}

imprimirBRL(-9.567);

ou */

let moedas = ( function(){
    //Tudo é privado
    function imprimirMoeda( params ){
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil( numero * fator ) / fator;
        }
    

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params

        let qtdCasasMilhares = 3
        let stringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero)%1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length

        let c =1
        while(parteInteiraString.length > 0) {
            if(c % qtdCasasMilhares == 0){
                stringBuffer.push( `${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c )}`) //o slice serve para pzssar qual a quantidade eu quero ter
                parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
            }else if(parteInteiraString.length < qtdCasasMilhares){
                stringBuffer.push( parteInteiraString )
                parteInteiraString = ''
            }
            c++
        }

        stringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace( '0.', '').padStart(2,0)
        numeroFormatado = `${ stringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado))

       //return stringBuffer

    }

    //Tudo é público, ou seja , tudo que estiver dentro de return é publico para acesso
    return {
        imprimirBRL: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`
            })
        
    }
})()

console.log(moedas.imprimirBRL(10000));