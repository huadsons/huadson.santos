const pokeApi = new PokeApi();

async function createCard( pokemon ) {
  // receber pokemon, pegar valores do objeto e colocar nos lugares correto
  const nome = document.querySelector( '.pokemon-resultado h1' );
  nome.innerHTML = pokemon == null ? '' : `Nome: ${ pokemon.nome }`;
  const id = document.querySelector( ' .pokemon-resultado .id-pokemon' );
  id.innerHTML = pokemon == null ? '' : `Id: ${ pokemon.id }`;
  const tipo = document.querySelector( '.pokemon-resultado .tipo-pokemon' );
  tipo.innerHTML = pokemon == null ? '' : `Tipo: ${ pokemon.tipo }`;
  const estatisticas = document.querySelector( '.pokemon-resultado .estatisticas-pokemon' );
  estatisticas.innerHTML = pokemon == null ? '' : `Estatísticas:  ${ pokemon.estatisticas }`;
  const peso = document.querySelector( '.pokemon-resultado .peso-pokemon' );
  peso.innerHTML = pokemon == null ? '' : `Peso: ${ pokemon.peso } Kg`;
  const altura = document.querySelector( '.pokemon-resultado .altura-pokemon' );
  altura.innerHTML = pokemon == null ? '' : `Altura: ${ pokemon.altura } cm`;
}

function colocaImagem( pokemon ) {
  const imgPokemon = document.querySelector( '.pokemon-imagem img' );
  imgPokemon.src = pokemon == null ? 'https://pa1.narvii.com/6197/1252114c79d50f1c7d8a49722a51e2f830378119_hq.gif' : pokemon.imagem;
}

async function buscar( id ) {
  const pokemonEspecifico = await pokeApi.busca( id );
  const poke = new Pokemon( pokemonEspecifico );
  await createCard( poke );
  await colocaImagem( poke );
}

const numeros = [];
// salve no cache
function sortear() {
  const msgErro = document.getElementById( 'msg-erro' );
  if ( numeros.length < 802 ) {
    const aleatorio = Math.floor( Math.random() * ( 801 + 1 ) );
    if ( numeros.indexOf( aleatorio ) === -1 ) {
      numeros.push( aleatorio );
      msgErro.innerHTML = '';
      buscar( aleatorio );
    }
  } else {
    msgErro.innerHTML = 'Acabou';
    createCard( null );
    colocaImagem( null );
  }
}

const inputBusca = document.getElementById( 'numero' );
function validacaoNumeroDigitado() {
  const numDigitado = inputBusca.value;
  const msgErro = document.getElementById( 'msg-erro' );
  const seValido = ( numDigitado > 0 && numDigitado < 807 );
  if ( seValido ) {
    msgErro.innerHTML = '';
    const numDigitadoValido = inputBusca.value;
    buscar( numDigitadoValido );
  } else {
    msgErro.innerHTML = 'Insira um id valido';
    createCard( null );
    colocaImagem( null );
  }
}
inputBusca.addEventListener( 'blur', () => { validacaoNumeroDigitado() } );

const sorte = document.querySelector( '.sorte' )
sorte.addEventListener( 'click', event => {
  event.preventDefault();
  sortear();
} );
