class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name;
    this.id = obj.id;
    this.imagem = obj.sprites.front_default;
    this.tipo = obj.types.map( type => type.type.name ).join( ', ' );
    this.estatisticas = obj.stats.map( stat => `${ stat.stat.name } = ${ stat.base_stat }` ).join( '<br>' )
    this.peso = obj.weight / 10;
    this.altura = obj.height * 10;
    this.imgErro = 'https://pa1.narvii.com/6197/1252114c79d50f1c7d8a49722a51e2f830378119_hq.gif';
  }
}
