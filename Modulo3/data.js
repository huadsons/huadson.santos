//por padrão o java script já tem data por padrão


let agora1 = new Date();
let agora = new Date( 1994, 3, 15 );

console.log(agora);
console.log(typeof agora); // usa a hora do back, ou seja, do servidor
console.log(agora.getMonth()) //bucar algo(.get), neste caso pega o mês; 
console.log( `Mês formatado: ${ agora.getMonth() + 1}` ) //adciona 1 pq, por default comeca em 0(zero)
console.log( agora.getUTCFullYear() ) //ano com quatro casas
console.log( agora.getDay() );
console.log( agora.getDate() );
console.log( agora.getTime() ); //especificando horario exato