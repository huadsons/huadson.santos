function criarSanduiche( pao, recheio, queijo, salada ){
    console.log(`Seu Sanduiche tem o pão ${pao}
    com recheio de ${recheio}
    e queijo ${queijo}com
    salada de ${salada}`);
}


const ingredientes = ['3 queijos', 'Frango', 'Cheddar', 'Tomate e Alface'];


/* chamado em 40 lugares
criarSanduiche(...ingredientes); */

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

//receberValoresIndefinidos(1, 2, 3, 4, 5, 6); // transforma em um array para poder ser trabalhado

console.log(..."Huadson"); //desmembra o nome transformando em uma string separados as letras
console.log({..."Huadson"}); //tranforma em um objeto;
console.log([..."Huadson"]); //transforma em um array;

// window or Document

let inputTeste = document.getElementById('campoTeste'); // quando eu quyero pegar um elemento usa-se .get, como estar buscando pelo ID não precisa colocar #
inputTeste.addEventListener('blur', () =>{ // para adcionar um evento
    alert("Obrigado");
}); 