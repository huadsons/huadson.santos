/*Crie uma classe Dwarf, para representar os pequenos grandes artífices de jóias, e que são inimigos 
dos elfos! Cada Dwarf terá um nome, uma quantidade de vida (que, ao nascer, será de 110.0).
Altere a classe Elfo para que os elfos apenas possam atirar flechas em objetos do tipo Dwarf.
Cada vez que um elfo atirar uma flecha em um Dwarf, além do elfo ganhar experiência e perder uma flecha,
o Dwarf perderá 10.0 unidades de vida. Escreva testes unitários para a solução e para todas regras do exercício. 

Os Dwarves finalmente poderão se defender! Agora um Dwarf tem um inventário e ganha um item escudo (uma unidade) ao nascer. 
Ao equipar o escudo (isso é feito de forma manual, ou seja, é preciso fazer o dwarf equipá-lo), o dwarf passa a tomar metade 
do dano (em cada chamada). */


public class Dwarf extends Personagem{
    
    
    public Dwarf( String nome ){
       super(nome); // construtor chamando o nome da forma set
       this.vida = 110.0;
       this.qtdDano = 10.0;
       this.ganharItem( new Item(1, "Escudo") );
       
    }
    
    public void equiparEscudo(){
        this.qtdDano = 5.0;
    }
    
}