

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest{
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for( int i = 0; i < 11; i++ ) {
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), 1e-9);

    }
    
    @Test
    public void dwarfPerdeTodaVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for( int i = 0; i < 11; i++ ) {
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
}
