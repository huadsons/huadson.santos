
/*
Chegou a hora! Crie um exército de Elfos que pode:

Alistar um elfo, desde que ele seja um ElfoVerde ou ElfoNoturno.
Buscar a lista de elfos de um determinado status (exemplo: saber quais elfos do exército estão recém criados).

OBS: exército é uma nova entidade no jogo, portanto crie uma nova...
OBS: quando alistamos alguém em um contingente quer dizer que aquela pessoa entrou na lista, ou seja…

 */
import java.util.*;

public class Exercito {
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>( Arrays.asList( ElfoVerde.class, ElfosNoturno.class ));
    
    private ArrayList<Elfo> elfos = new ArrayList<>();
    
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); // passa dois parametos
    
    public void alistar( Elfo elfo ){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        
        if( podeAlistar ){
            elfos.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get( elfo.getStatus() );
            if( elfoDoStatus == null ) {
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
    
    public ArrayList<Elfo> buscar( Status status ){
        return this.porStatus.get(status);
    }
    
    
    
    
  
}
