
public class Elfo extends Personagem {

    private int indiceFlecha;
    private static int qtdElfos;
    
    {
      indiceFlecha = 0;
    }
    
    
    public Elfo( String nome ){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar( new Item(2, "Flecha") );
        this.inventario.adicionar( new Item(1, "Arco") );
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha() {
        return this.inventario.ganhar(indiceFlecha);
    }
    
    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    public void atirarFlecha( Dwarf dwarf){
        if(this.getQtdFlecha() > 0){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            //this.experiencia = experiencia + 1;
            this.aumentaXp();
            dwarf.sofrerDano();
            this.sofrerDano();
        }
    }

}
