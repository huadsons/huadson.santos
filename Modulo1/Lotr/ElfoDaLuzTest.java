

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void nasceComEspada() {
        ElfoDaLuz elf = new ElfoDaLuz("elf");
        assertEquals(new Item(1, "Espada de Galvorn"),
        elf.getInventario().buscar("Espada de Galvorn"));
    }
    @Test
    public void atacaEGanha1Exp() {
        ElfoDaLuz elf = new ElfoDaLuz("elf");
        elf.atacarComEspada(new Dwarf("gimli"));
        assertEquals(1, elf.getExperiencia());
    }
    @Test
    public void ataca1xPerdeVida() {
        ElfoDaLuz elf = new ElfoDaLuz("elf");
        elf.atacarComEspada(new Dwarf("gimli"));
        assertEquals(79.0, elf.getVida(), 1e-9);
    }
    @Test
    public void ataca2xGanhaVida() {
        ElfoDaLuz elf = new ElfoDaLuz("elf");
        elf.atacarComEspada(new Dwarf("gimli"));
        elf.atacarComEspada(new Dwarf("gimli"));
        assertEquals(89.0, elf.getVida(), 1e-9);
    }
    
}