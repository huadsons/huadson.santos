/*
Crie uma classe chamada EstatisticasInventario, que recebe na sua construção um inventário e que possua os seguintes métodos:

calcularMedia: retorna a média de quantidades de itens do inventário.
calcularMediana: retorna a mediana das quantidades de itens do inventário.
qtdItensAcimaDaMedia: retorna a quantidade de itens que estão acima da média de quantidades
*/
public class EstatisticasInventario{
    
    private Inventario inventario;
    
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
   
    public double calcularMedia() {
        
        if (this.inventario.getItens().isEmpty()) { //compara pra saber se e vazio
            return Double.NaN;
        }
        
        double somaQtds = 0;
        for( Item item : this.inventario.getItens() ) {
            somaQtds += item.getQuantidade();
        }
        return somaQtds / inventario.getItens().size();
    }
    
    public double calcularMediana() {
        
        if (this.inventario.getItens().isEmpty()) { //compara pra saber se e vazio
            return Double.NaN;
        }
        
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.ganhar(meio).getQuantidade();
        if( qtdItens %2 == 1 ){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.ganhar( meio - 1).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int qtdAcima = 0;
        for( Item item : inventario.getItens() ) {
            if( item.getQuantidade() > media ) {
                qtdAcima++;
            }
        }
        return qtdAcima;
    }
}