
import java.util.*;

public class ComparadorDePerformance
{
    public void comparar(){
        ArrayList<Elfo> arrayElfos = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfos = new HashMap<>();
        int qtdElfos = 1500000;
        
        for( int i = 0; i < qtdElfos; i++ ){
            String nome = "Elfo "+ i;
            Elfo elfo = new Elfo(nome);
            arrayElfos.add(elfo);
            HashMApElfos.put(nome, elfo);
        }
    
        String nomeBusca = "Elfo 100000";
        
        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscaSequencial(arrayElfos, nomeBusca);
        long mSeqFim = System.currentTimeMillis();
        
        long mHashInicio = System.currentTimeMillis();
        Elfo elfoMap = buscaMap(hashMapElfos, nomeBusca);
        long mHasFim = System.currentTimeMillis();
        
        String tempoSeq = String.format("%.10f", (mSeqInicio));
    }
    
    //private Elfo buscaSequencial( ArrayList<Elfo> lista, String nome ){
        
    //}
}
