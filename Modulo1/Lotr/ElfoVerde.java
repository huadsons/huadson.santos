/*O novo patch do jogo permitirá o uso de Elfos Verdes! Eles ganham o dobro de experiência que os elfos normais ganham ao
 * atirar uma flecha e só podem ganhar ou perder itens com as seguintes descrições (note que assim como todos Elfos,
 * eles já nascem com Arco e Flecha):
"Espada de aço valiriano"
"Arco de Vidro"
"Flecha de Vidro"
Não se preocupe com os elfos normais, com a chegada dos Elfos Verdes eles não ficarão extintos.

 */
import java.util.*;

public class ElfoVerde extends Elfo {

    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de viro"
        )
    );
    public ElfoVerde( String nome ) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
    
    @Override
    public void ganharItem( Item item ) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida) {
            this.inventario.adicionar(item);
        }
    }
    
    @Override
    public void perderItem( Item item ) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida) {
            this.inventario.perder(item);
        }
    }
    

}