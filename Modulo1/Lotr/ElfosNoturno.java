
/*Elfos Noturnos ganham o triplo de experiência de um Elfo normal ao atirar uma flecha,
mas perdem 15 unidades de vida para cada flecha atirada. */
public class ElfosNoturno extends Elfo {
    
    public ElfosNoturno( String nome ) {
        super(nome);
        this.qtdExperienciaPorAtaque = 3;
        this.qtdDano = 15.0;
    }
    
    /*@Override
    public void atirarFlecha( Dwarf dwarf ){
        
        if(podeSofrerDano()){
        this.vida = this.vida > 15.0 ? this.vida - 15.0 : 0.0; 
            if( this.vida == 0.0 ) {
                this.status = Status.MORTO;
            }
        }
    }*/
}