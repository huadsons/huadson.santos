
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
    @Test
    public void criarInventarioSemQuantidadeInformada(){
        Inventario inventario = new Inventario(2);
        
    }



    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        //assertEquals(espada, inventario.getItens());
        assertEquals(espada, inventario.ganhar(0));
    }
    
    /*@Test
    public void adicionarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        //assertEquals(espada, inventario.getItens()[0]);
        assertEquals(espada, inventario.obter(0));
        //assertEquals(escudo, inventario.getItens()[0]);
        assertEquals(escudo, inventario.obter(1));
    }*/
    
    
    /*@Test
    public void obterItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }*/
    
    @Test
    public void removerUmItem() {
        Inventario inventario = new Inventario(11);
        Item arco = new Item( 1, "Arco" );
        Item flecha = new Item( 2, "Flecha" );
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.perder(arco);
        assertEquals( inventario.buscar( "Arco" ) , null );
    }
    
    @Test
    public void getDescircoesVariosItens(){
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item lanca = new Item(1, "Lança");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        assertEquals("Espada,Lança,Escudo", inventario.getDescricoesItens());
        
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(1);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void inverterInventarioComUmItem(){
        Inventario inventario = new Inventario(1);
        Item arco = new Item(1, "Arco");
        inventario.adicionar(arco);
        assertEquals(arco, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    }
    
    @Test
    public void inverterInventarioDoisItens(){
        Inventario inventario = new Inventario(1);
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(1, "Flecha");
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        assertEquals(flecha, inventario.inverter().get(0));
        assertEquals(arco, inventario.inverter().get(1));
        assertEquals(2, inventario.inverter().size());
    }

}


