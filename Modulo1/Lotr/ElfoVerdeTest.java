

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void elfoVerdeGanha2XPorFlecha(){
        ElfoVerde celebron = new ElfoVerde("Celebron");  //Tbm pode ser: new Elfo pois ele herda de Elfo
        celebron.atirarFlecha(new Dwarf("Balin"));
        assertEquals(2, celebron.getExperiencia());
    }
    
    @Test
    public void elfoVerdeAdicionarItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.ganhar(0));
        assertEquals(new Item(1, "Arco"), inventario.ganhar(1));        
        assertEquals(arcoDeVidro, inventario.ganhar(2));
    }
    
    @Test
    public void elfoVerdeAdicionarItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.ganhar(0));
        assertEquals(new Item(1, "Arco"), inventario.ganhar(1));        
        assertNull(inventario.buscar("Arco de Madeira"));
    }
    
    
}
