/**
Crie uma classe chamada Inventario, que representará toda mochila de itens dos Elfos (ou seja, terá um array de Item).
Caso seja informado um número, na construção do Inventario, este deverá ser o tamanho do array de Item. Caso não seja
informado número, o array terá inicialmente 99 slots para itens. Inicialmente o Inventário possuirá três operações básicas:

adicionar(Item) - recebe uma instância de item e adiciona no primeiro slot que tiver referência null

obter(posicao) - retorna o item correspondente àquela posição no array

remover(posicao) - remove o item correspondente àquela posição no array (setando para null sua referência)

Faça as alterações necessárias na classe Elfo para que ela não tenha mais dois atributos de Item, e sim um inventário
com os dois itens que sabemos que eles possuem (arco e flecha, mesmas quantidades).
 */

import java.util.*;


public class Inventario {
   
   private ArrayList<Item> itens;
    
   public Inventario(int quantidade) {
       this.itens = new ArrayList<Item>(quantidade);
   }  
   
   public ArrayList<Item> getItens(){
     return this.itens;  
   }
   
    public Item ganhar( int posicao ) {
       if( posicao >= this.itens.size() ){
           return null;
       }
       return this.itens.get(posicao);
   }
   
   public void perder( Item item ) {
       this.itens.remove(item);
   }
   
   public void adicionar( Item item ) {
       this.itens.add(item);
   }
   
   public String getDescricoesItens() {
       StringBuilder descricoes = new StringBuilder();
       for ( int i = 0; i < this.itens.size(); i++ ){
           Item item = this.itens.get(i);
               descricoes.append(item.getDescricao());
               descricoes.append(",");
       }
       
       return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());
   }
   
   public Item getItemComMaiorQuantidade() {
       int indice = 0, maiorQuantidade = 0;
       
       for ( int i = 0; i< this.itens.size(); i++ ){
           
           Item item = this.itens.get(i);
           
               if ( item.getQuantidade() > maiorQuantidade ){
                   maiorQuantidade = item.getQuantidade();
                   indice = i;
                }
           
       }
       return this.itens.size() > 0 ? this.itens.get(indice) : null;
   }
   
   public Item buscar ( String descricao ) {
       for ( Item itemAtual : this.itens ) {
           boolean encontrei = itemAtual.getDescricao().equals(descricao);
                if( encontrei ){
                    return itemAtual;
                }
       }
       return null;
   }
   
   public ArrayList<Item> inverter(){
       ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
       
       for( int i = this.itens.size() - 1; i>=0 ; i-- ) {
           listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
   }
   
   public void ordenarItens(){
       this.ordenarItens(tipoOrdenacao.ASC);
   }
   
   public void ordenarItens(tipoOrdenacao ordenacao){
       for ( int i = 0; i < this.itens.size(); i++ ){
           for ( int j = 0; j < this.itens.size(); j++  ){
               Item atual = this.itens.get(j);
               Item proximo = this.itens.get(j + 1);
               
               boolean deveTrocar = ordenacao == tipoOrdenacao.ASC ?
               atual.getQuantidade() > proximo.getQuantidade() : 
               atual.getQuantidade() < proximo.getQuantidade();
               
               if( deveTrocar ){
                   Item itemTrocado = atual;
                   this.itens.set(j, proximo);
                   this.itens.set(j + 1, itemTrocado);
               }
           }
       }
   }
   
   public Inventario unir ( Inventario novoInventario ){
       Inventario novo = this;
       for( Item item : novoInventario.getItens() ){
           if( !novo.getItens().contains( item ) ){
               novoInventario.adicionar(item);
           }
       }
       return novoInventario;
   }
   
   public Inventario diferenciar( Inventario outroInventario ){
       Inventario outro = this;
       for( Item item : outroInventario.getItens() ){
           if( outro.getItens().contains( item ) ){
               outroInventario.perder(item);
           }
       }
       return outroInventario;
   }
    
}