
/*Os ElfoDaLuz (Calaquendi, em élfico), vieram de longe (Aman) e chegaram no nosso jogo, na Terra Média!
 * Implemente sua classe e permita que eles nasçam com uma espada de Galvorn (resistente metal élfico
 * criado nas Montanhas Azuis, durante a Primeira Era) e que possam atacar dwarves com essa espada, sem que
 * ela quebre ou seja perdida do inventário. Porém, a cada ataque de número ímpar, o ElfoDaLuz perde 21 unidades de vida,
 * e cada ataque de número par, ganha 10 unidades de vida. Elfos da Luz já nascem com arco e flecha e
 * ganham 1 de experiência no ataque (como elfos). Exemplo de uso:

    ElfoDaLuz feanor = new ElfoDaLuz(“Fëanor”);
    feanor.atacarComEspada(new Dwarf(“Farlum”)); // perde 21 unidades, foi o ataque 1
    feanor.getVida(); // 79.0
    feanor.atacarComEspada(new Dwarf(“Gul”)); // ganha 10 unidades, foi o ataque 2
    feanor.getVida(); // 89.0

 */
import java.util.*;

public class ElfoDaLuz extends Elfo {
    
    private int qtdAtaques;
    private final double QTD_VIDA_GANHA;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList("Espada de galvorn"));
    
    {
        qtdAtaques = 0;
        QTD_VIDA_GANHA = 10;
    }
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21.0;
    }
    
    public boolean devePerderVida(){
        return qtdAtaques % 2 == 1;   
    }
    
    public void ganharVida(){
        vida +=QTD_VIDA_GANHA;
    }
    
    public void perderItem( Item item ){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder){
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada( Dwarf dwarf ) {
        
        if( this.getStatus() != Status.MORTO ) {
            dwarf.sofrerDano();
            this.aumentaXp();
            qtdAtaques++;
            if( devePerderVida()) {
                this.sofrerDano();
            }
            else {
                this.ganharVida();
            }
        }
    }
}