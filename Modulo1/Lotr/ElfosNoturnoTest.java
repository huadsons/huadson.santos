
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfosNoturnoTest {
    
    @Test
    public void ganha3ExpPerde15DeVida() {
        ElfosNoturno elfosNoturno = new ElfosNoturno("elfo noturno");
        elfosNoturno.atirarFlecha(new Dwarf("gimli"));
        assertEquals(3, elfosNoturno.getExperiencia());
        assertEquals(85.0, elfosNoturno.getVida(), 1e-9); 
    }
    
    @Test
    public void atiraDuasflechas() {
        ElfosNoturno elfosNoturno = new ElfosNoturno("elfo noturno");
        for(int i = 0; i < 2; i++) {
            elfosNoturno.atirarFlecha(new Dwarf("gimli"));
        }
        assertEquals(70.0, elfosNoturno.getVida(), 1e-9); 
    }
    
    @Test
    public void atiraFlechaEMorre() {
        ElfosNoturno elfosNoturno = new ElfosNoturno("elfo noturno");
        elfosNoturno.getFlecha().setQuantidade(1000);
        for(int i = 0 ; i < 7; i++) {
            elfosNoturno.atirarFlecha(new Dwarf("dwarf"));
        }
        assertEquals(0.0, elfosNoturno.getVida(), 1e-9);
        assertEquals(Status.MORTO, elfosNoturno.getStatus());
    }
}
