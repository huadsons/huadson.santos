public class TradutorParaEspanhol implements Tradutor
{
    public String traduzir( String textoEmPortugues ){
        switch (textoEmPortugues){
            case "Sim":
                return "Si";
                case "Obrigado":
            case "Obrigada":
                return "Gracias";
            default:
                return null;
        }
    }
}
