/*Crie um projeto de ExercíciosExtra do BlueJ, crie uma classe AgendaContatos que pode:

adicionar um contato (nome e telefone)
obter o telefone a partir de um nome
pesquisar um contato a partir do telefone (sem criar um outro hashmap)
Desafio: retornar uma String em formato CSV (mais informações no link) e ordenada de A-Z, exemplo:

    AgendaContatos agenda = new AgendaContatos();
    agenda.adicionar(“Marcos”, “555555”);
    agenda.adicionar(“Mithrandir”, “444444”);
    agenda.consultar(“Marcos”); // “555555”
    agenda.csv(); // retorna a seguinte string:
 */

import java.util.*;

public class AgendaContatos {
    private HashMap<String, String> contatos;
    
    public AgendaContatos(){
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String telefone){
        contatos.put( nome, telefone );
    }
    
    public String consultar( String nome ){
        return contatos.get(nome);
    }
    
    public String consultarPorTelefone( String telefone ){
        for( HashMap.Entry<String, String> par : contatos.entrySet() ){
            if( par.getValue().equals(telefone) ){
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for( HashMap.Entry<String, String> par : contatos.entrySet() ){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador );
            builder.append(contato);
        }
        return builder.toString();
    }
}


