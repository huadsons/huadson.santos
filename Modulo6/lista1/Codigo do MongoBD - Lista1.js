db.Turismo.insert(
    {
        Cidade: "Itabuna-BA",
        AnoDeFundação: 1910,
        DistanciaDePoa: 2784,
        PontosTuristícos:
        [
            {
                Nome: "Catedral de São José",
                ValorDeEntrada: 0.00,
                Comentário: "Local de paz e contemplação, as missas de quartas feiras às 18:00 são fantásticas, realmente muito boas."
            },
            {
                Nome: "Centro De Reabilitacao Reserva Zoobotanica",
                ValorDeEntrada: 0.00,
                Descrição: "Um projeto da faculdade que também pesquisa o cacau no local."
            }
        ]
    }
)

db.Turismo.insert(
    {
        Cidade: "Gramado-RS",
        AnoDeFundação: 1954,
        DistanciaDePoa: 108,
        PontosTuristícos:
        [
            {
                Nome: "Igreja São Pedro",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "7h30 às 21 horas",
                Endereço: "Av. Borges de Medeiros, 2659, Centro"
            },
            {
                Nome: "Lago Negro",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "acesso livre (24 horas)",
                Pedalinhos: "Diariamente, de 8h30 às 18h"
            },
            {
                Nome: "Caro Watson",
                ValorDeEntrada: 239.60,
                HorárioDeFuncionamento: "Diariamente das 14h às 20h",
                Curiosidade: "O DiamanteRoubar é errado. Na maioria das vezes. Essa sala guarda o maior diamante do mundo. O guarda foi subornado e conseguiu desativar o sistema de alarme por 60 minutos. Sua equipe tem uma hora para botar a mão nessa jóia antes que o alarme dispare e a polícia chegue."
            }
        ]
    }
)


db.Turismo.insert(
    {
        Cidade: "Santa Vitória do Palmar",
        AnoDeFundação: 1874,
        DistanciaDePoa: 500,
        PontosTuristícos:
        [
            {
                Nome: "Loja Maçônica Acácia Vitoriense",
                ValorDeEntrada: 0.00,
                Descricao: "O prédio da antiga loja acácia vitoriense se encontra em ótimo estado de preservação, sendo utilizado até hoje para reuniões da maçonaria. O turista não tem acesso ao prédio, podendo visualizá-lo apenas por fora, mas vale a pena a visita, pois a arquitetura é típica da região",
                Construção: "1873"
            },
            {
                Nome: "Teatro Independência",
                ValorDeEntrada: 0.00,
                Endereço: "Rua Conde de Porto Alegre, 236",
                contemplação: "O Teatro Independência é uma joia da cultura brasileira. Sua arquitetura e história me encantaram e me fizeram um artista de nossa terra. Um orgulho Mergulhão."
            },
            {
                Nome: "Igreja Santa Vitória",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "Diariamente das 08h às 21h",
                Descrição: "Igreja Santa Vitória na frente da Praça General Andréa, também faz vizinhança com o Teatro Independência. Tem 2 torres, relógios, sinos e cruz"
            },
            {
                Nome: "Praia do Hermenegildo",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "Aberto todos os dias",
                Descrição: "Tranquilo, ideal para descandar da correria da cidade. Ainda que modesto possui estrutura para que nao lhe falte nada durante a estadia."
            }
        ]
    }
)

db.Turismo.insert(
    {
        Cidade: "Ilhéus",
        AnoDeFundação: 1536,
        DistanciaDePoa: 2815,
        PontosTuristícos:
        [
            {
                Nome: " Casa de Cultura Jorge Amado",
                ValorDeEntrada: "Não pode ser acessado seu interior",
                Descricao: "A importância desse casarão construído pelo Coronel João Amado, pai do escrito Jorge Amado, é que o celebre escritor passou ali parte de sua vida e foi onde escreveu o romance “O País do Carnaval” de 1931.",
                ComoChegar: "Localizada na Rua Jorge Amado, 21 – Parte histórica da cidade de Ilhéus."
            },
            {
                Nome: "Catedral São Sebastião",
                Inauguração: "inaugurada em 1967",
                Contemplação: "reúne em sua fachada detalhes minuciosos do estilo neoclássico, como vitrais artísticos, abóbadas e colunas. O exterior majestoso contrasta com o interior, bastante discreto e singelo."
            },
            {
                Nome: "Cine Teatro Municipal de Ilhéus",
                Fundação: "fundado em 1932",
                Descrição: "originalmente chamado Cine-Teatro Ilhéos, é uma das mais antigas e tradicionais casas de espetáculos da Bahia. Localizado na tradicional Praça Luiz Viana Filho"
            },
            {
                Nome: "Lagoa Encantada",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "Aberto todos os dias",
                Descrição: "Envolvida área de proteção ambiental. Além da paisagem formada por um espelho d’água 6,4 Km², cercado por fazendas e mata nativa. As ilhas flutuantes despertam a curiosidade, elas movem-se de um lado a outro da lagoa, a depender do vento.",
                ComoChegar: "Saindo pela BA – 001 – Ilhéus, em direção ao litoral Norte, pecorre-se 14 km. Nas imediações da Praia do Jóia, entrar à esquerda. Percorrer 1 km em estrada de terra até o Clube Náutilus, no condomínio Jóia do Atlântico, ponto de apoio para o passeio."
            },
            {
                Nome: "Engenho Santana",
                ValorDeEntrada: 0.00,
                HorárioDeFuncionamento: "Aberto todos os dias",
                Descrição: "Navegando no rio do Engenho você avista a Av. Dois de Julho, antiga zona portuária e comercial de Ilhéus, hoje ocupada por diversos bares e restaurantes. Seguindo o curso do rio encontra-se a Ponte Lomanto Junior, que liga Ilhéus ao bairro do Pontal, logo após, a enseada de Sapetinga e o canal do Fundão. Nessa altura a o encontro dos rios Engenho, com o rio Cachoeira e o rio Fundão. No decorrer do trajeto ainda encontra-se a Capela de Santana, na vila do Rio Engenho, e o engenho de Santana, ainda com peças do antigo engenho, como tacho de ferro e a armação de pedra onde a água saltava sobre a roda d’água ainda podem ser vista.",
                ComoChegar: "O embarque é na Praça Maramata (bairro do Pontal/Ilhéus). Segue-se de chalana (embarcação de fundo chato) pelo rio Engenho.até o povoado de Engenho de Santana."
            },
            {
                Nome: "Fazenda Santo Antônio/ pesque pague eco-água",
                ValorDeEntrada: 0.00,
                Descrição: "Com opções de lazer para toda família, o Pesque Pague Eco-Água dispõe de completa infra-estrutura para o “day use” em fazenda e oferece também a possibilidade de realização de eventos.",
            }

        ]
    }
)


db.Turismo.insert(
    {
        Cidade: "Porto Seguro",
        AnoDeFundação: 1945,
        DistanciaDePoa: 2725,
        PontosTuristícos:
        [
            {
                Nome: " Praia do espelho",
                ValorDeEntrada: 0.00,
                Descrição: "A Praia do Espelho, localizada a 12km de Caraíva, é uma praia de difícil acesso, o que a faz um refúgio da vida moderna e dos problemas do dia a dia.",
            },
            {
                Nome: "Praia de Caraíva",
                HorárioDeFuncionamento: "Os barracas e praias funcionam durante o dia e a noite, o que torna o local um point diurno e noturno perfeito.",
                Contemplação: "Suas areias fofas e o mar de água limpa e cristalina protegido por belos recifes chamam a atenção dos turistas que querem desfrutar de uma das mais belas vistas que Porto Seguro pode oferecer."
            },
            {
                Nome: "Museu de Porto Seguro",
                Localização: "Próxima a rodoviária o museu de Porto Seguro oferece uma vista única do passado do nosso país e da nossa rica história.",
                Descrição: "O local é uma antiga cadeia e câmera que foi convertida em museu e possui uma exposição completa retratando a vida dos indígenas pré-colonização."
            }
        ]
    }
)


db.Turismo.insert(
    {
        Cidade: "Bento Gonçalves",
        AnoDeFundação: 1890,
        DistanciaDePoa: 122,
        PontosTuristícos:
        [
            {
                Nome: "Vinícola Aurora",
                ValorDeEntrada: 389.90,
                Fundação: "A história da AURORA inicia em 1875",
                Descrição: "com a chegada de imigrantes oriundos do norte da Itália. Estabelecidos no Sul do Brasil, na Serra Gaúcha, onde encontraram paisagens e clima similares aos de seu país de origem. Assim, os hábitos e a cultura européia não foram abandonados, e a antiga arte da vitivinicultura logo foi retomada.",
            },
            {
                Nome: "Caminhos de Pedra",
                ValorDeEntrada: 690.00,
                Fundação: "É um empreendimento do ano de 2000",
                Contemplação: "A construção é típica da arquitetura italiana com porão totalmente de pedra basalto.Sem sustentação de vigas e sem liga de cimento. Os arcos das portas e janelas são da arquitetura etrusca-romana. A parte superior de alvenaria com tijolos maciços e abriga descendentes de imigrantes italiano de Belluno que aqui chegaram em 1875."
            }
        ]
    }
)


db.Turismo.updateOne({Cidade: "Porto Seguro"}, {$push: {DistanciaDeSalvador: "600 km"}})
db.Turismo.updateOne({Cidade: "Santa Vitória do Palmar"}, {$pull: {DistanciaDePoa: 500}})
db.Turismo.updateOne({"_id" : ObjectId("5e4458ab6e07eb815e08ec19")}, {$set: {AnoDeFundação: "11 de outubro de 1890"}})

db.Turismo.remove({"_id" : ObjectId("5e44586f6e07eb815e08ec16")})

db.Turismo.find({$and:[{"PontosTuristícos.ValorDeEntrada":0.00}, {"DistanciaDePoa":{$gte:2000}}]}).pretty()
db.Turismo.find({"PontosTuristícos.ValorDeEntrada": 0, "DistanciaDePoa": {$gte: 1000}}).pretty()

db.Turismo.find({nome: { $regex: /Ben/ }, ValorDeEntrada: {$gte: 600}})

db.Turismo.find().skip(1).limit(3)

sudo ./mongodump --out /home/vemser/Documentos/Huadson/huadson.santos/Modulo6/mongodb-linux/backup/

db.Turismo.renameCollection("Tuturismo")
