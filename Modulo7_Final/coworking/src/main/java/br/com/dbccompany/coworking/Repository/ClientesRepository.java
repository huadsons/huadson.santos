package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
    Optional<Clientes> findById (Integer id);
    List<Clientes> findAll();

}
