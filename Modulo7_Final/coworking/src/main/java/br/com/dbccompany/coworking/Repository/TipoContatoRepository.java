package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer> {
    Optional<TipoContato> findById (Integer id);
    List<TipoContato> findAll();
}
