package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Espacos salvar(Espacos espacos ) {
        return repository.save( espacos );
    }

    public List<Espacos> todosEspacos() {
        return repository.findAll();
    }

    public Espacos espacoEspecifico( Integer id ){
        Optional<Espacos> espacos = repository.findById( id );
        return espacos.get();
    }

    public void delete( Integer id ){

        repository.deleteById( id );
    }
}
