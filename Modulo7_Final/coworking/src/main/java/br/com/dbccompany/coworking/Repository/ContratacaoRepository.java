package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contatos;
import br.com.dbccompany.coworking.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {
    Optional<Contratacao> findById (Integer id);
    List<Contratacao> findAll();
}