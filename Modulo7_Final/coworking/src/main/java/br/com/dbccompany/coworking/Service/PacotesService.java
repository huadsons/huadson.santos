package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacotes ) {
        return repository.save( pacotes );
    }

    public List<Pacotes> todosPacotes() {
        return repository.findAll();
    }

    public Pacotes pacoteEspecifico( Integer id ){
        Optional<Pacotes> pacotes = repository.findById( id );
        return pacotes.get();
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
