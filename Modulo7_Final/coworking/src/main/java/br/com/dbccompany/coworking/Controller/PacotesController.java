package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "api/pacotes")
public class PacotesController {

    @Autowired
    private PacotesService service;

    @PostMapping( value = "novo")
    @ResponseBody
    public Pacotes novoPacote(@RequestBody Pacotes pacotes ){
        return service.salvar( pacotes );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pacotes buscaPorIdPacote( @PathVariable Integer id ){
        return service.pacoteEspecifico( id );
    }

}
