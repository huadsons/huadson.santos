package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {
    Optional<Usuarios> findById (Integer id);
    List<Usuarios> findAll();
}
