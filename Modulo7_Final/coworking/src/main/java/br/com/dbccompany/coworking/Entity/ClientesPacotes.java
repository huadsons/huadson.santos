package br.com.dbccompany.coworking.Entity;
import javax.persistence.*;

@Entity
@Table( name = "CLIENTES_X_PACOTES")
public class ClientesPacotes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ" )
    @GeneratedValue( generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CLIENTES_PACOTES", nullable = false)
    private Integer id;

    private Integer quantidades;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTES", nullable = false)
    private Clientes clientes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTE", nullable = false)
    private Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidades() {
        return quantidades;
    }

    public void setQuantidades(Integer quantidades) {
        this.quantidades = quantidades;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}
