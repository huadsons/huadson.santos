package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "CLIENTES")
public class Clientes {

    @Id
    @SequenceGenerator (allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
    @GeneratedValue (generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CLIENTES", nullable = false)
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true )
    private String cpf;

    @Column( nullable = false )
    private Date dataNascimento;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CONTATOS", nullable = false)
    private Contatos contatos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Contatos getContatos() {
        return contatos;
    }

    public void setContatos(Contatos contatos) {
        this.contatos = contatos;
    }
}
