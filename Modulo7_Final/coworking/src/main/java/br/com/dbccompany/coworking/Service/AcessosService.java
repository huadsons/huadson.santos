package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Acessos salvar( Acessos acessos ) {
        return repository.save( acessos );
    }

    public List<Acessos> todosAcessos() {
        return repository.findAll();
    }

    public Acessos acessoEspecifico( Integer id ){
        Optional<Acessos> acessos = repository.findById( id );
        return acessos.get();
    }

    public void delete( Integer id ){

       repository.deleteById( id );
    }

    public Acessos dataEspecifica(Date date) {
        Optional<Acessos> acessos = repository.findByData( date );
        return acessos.get();
    }
}