package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
    Optional<Espacos> findById (Integer id);
    List<Espacos> findAll();
}
