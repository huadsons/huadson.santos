package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Integer> {
    Optional<SaldoCliente> findById (Integer id);
    List<SaldoCliente> findAll();
}