package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Clientes salvar(Clientes clientes ) {
        return repository.save( clientes );
    }

    public List<Clientes> todosClientes() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes ClienteEspecifico( Integer id ){
        Optional<Clientes> clientes = repository.findById( id );
        return clientes.get();
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }

    public Clientes editar(Integer id, Clientes cliente) {
        Optional<Clientes> clientes = repository.findById( id );
        return clientes.get();
    }

    public void deletar(Integer id) {
            repository.deleteById( id );
        }

    public Clientes clienteEspecifico(Integer id) {
        Optional<Clientes> clientes = repository.findById( id );
        return clientes.get();
    }
}
