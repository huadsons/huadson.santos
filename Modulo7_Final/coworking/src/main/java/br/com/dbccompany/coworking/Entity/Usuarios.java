package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "USUARIOS" )
public class Usuarios {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ" )
    @GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_USUARIOS", nullable = false)
    private Integer id;

    private String nome;

    @Column( unique = true)
    private String email;

    @Column( unique = true)
    private String login;

    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
