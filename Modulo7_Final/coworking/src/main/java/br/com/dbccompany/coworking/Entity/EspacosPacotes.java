package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESPACOS_X_PACOTES")
public class EspacosPacotes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS_PACOTES", nullable = false)
    private Integer id;

    private TipoContratacao tipoContratacao;

    private Integer quantidade;

    private Integer prazo;

    @ManyToOne
    @JoinColumn( name = "ID_ESPACOS", nullable = false)
    private Espacos espacos;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTES", nullable = false)
    private Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}
