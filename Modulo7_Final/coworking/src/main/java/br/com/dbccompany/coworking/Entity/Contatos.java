package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATOS")
public class Contatos {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTATOS_SEQ", sequenceName = "CONTATOS_SEQ" )
    @GeneratedValue( generator = "CONTATOSS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CONTATOS")
    private Integer id;

    @Column(nullable = false)
    private String valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_TIPO_CONTATO", nullable = false)
    private TipoContato tipoContato;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_CLIENTES", nullable = false )
    private Clientes clientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

}
