package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {
    Optional<EspacosPacotes> findById (Integer id);
    List<EspacosPacotes> findAll();
}
