package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "ESPACOS")
public class Espacos {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS", nullable = false)
    private Integer id;

    @Column( nullable = false, length = 50, unique = true )
    private String nome;

    @Column( nullable = false)
    private Integer qtdPessoas;

    @Column( nullable = false)
    private Double valor;

    @OneToMany( mappedBy = "espacos")
    Set<SaldoCliente> saldoCliente;

    @OneToMany(mappedBy = "espacos")
    Set<EspacosPacotes> espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Set<SaldoCliente> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Set<SaldoCliente> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Set<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(Set<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }
}
