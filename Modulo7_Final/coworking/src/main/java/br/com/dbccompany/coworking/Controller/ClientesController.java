package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/cliente")
public class ClientesController {

    @Autowired
    private ClientesService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes adicionarCliente(@RequestBody Clientes cliente ) {
        return service.salvar(cliente);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Clientes editarCliente(@PathVariable Integer id, @RequestBody Clientes cliente) {
        return service.editar( id, cliente);
    }
    @RequestMapping( value = "/deleta/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarCliente( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Clientes buscarClienteId(@PathVariable Integer id) {

        return service.clienteEspecifico(id);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Clientes> todosClientes(){

        return service.todosClientes();
    }

}
