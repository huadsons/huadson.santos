package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
    Optional<Acessos> findByData(Date date);
    List<Acessos> findAll();
}
