package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contatos;
import br.com.dbccompany.coworking.Repository.ContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatosService {
    @Autowired
    private ContatosRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Contatos salvar(Contatos contatos ) {
        return repository.save( contatos );
    }

    public List<Contatos> todosContatos() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public Contatos ContatoEspecifico( Integer id ){
        Optional<Contatos> contatos = repository.findById( id );
        return contatos.get();
    }

    public void delete( Integer id ){

        repository.deleteById( id );
    }

    @Transactional( rollbackFor = Exception.class)
    public Contatos editar(Integer id, Optional<Contatos> contatos) {
        contatos = repository.findById( id );
        return contatos.get();
    }

    public void deletar(Integer id) {
        repository.deleteById( id );
    }

    public Contatos contatoEspecifico(Integer id) {
        Optional<Contatos> contatos = repository.findById( id );
        return contatos.get();
    }
}
