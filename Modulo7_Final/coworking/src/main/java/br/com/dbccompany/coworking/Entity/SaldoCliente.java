package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALDO_X_CLIENTE")
public class SaldoCliente {
    @EmbeddedId
    private SaldoClienteId id;

    private TipoContratacao tipoContratacao;

    private Integer quantidade;

    private Date vencimento;

    @ManyToOne
    @MapsId ( "ID_CLIENTES" )
    @JoinColumn( name = "ID_CLIENTES")
    private Clientes clientes;

    @ManyToOne
    @MapsId( "ID_ESPACOS" )
    @JoinColumn( name = "ID_ESPACOS", nullable = false)
    private Espacos espacos;

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
