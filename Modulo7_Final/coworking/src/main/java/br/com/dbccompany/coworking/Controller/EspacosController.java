package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "api/espacos")
public class EspacosController {

        @Autowired
        private EspacosService service;

        @PostMapping( value = "novo")
        @ResponseBody
        public Espacos novoEspaco(@RequestBody Espacos espacos ){

            return service.salvar( espacos );
        }

        @GetMapping( value = "/{id}" )
        @ResponseBody
        public Espacos buscaPorIdEspaco( @PathVariable Integer id ){
            return service.espacoEspecifico( id );
        }
}
