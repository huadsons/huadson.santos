package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping( value = "api/acesso")
public class AcessosController {

    @Autowired
    private AcessosService service;

    @PostMapping( value = "novo")
    @ResponseBody
    public Acessos novoAcesso(@RequestBody Acessos acessos ){

        return service.salvar( acessos );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Acessos buscaPorIdAcesso( @PathVariable Integer id ){

        return service.acessoEspecifico( id );
    }

    @GetMapping( value = "/data/{data}")
    @ResponseBody
    public Acessos buscaPorDataAcesso(@PathVariable Date date ){
        return service.dataEspecifica( date );
    }

}
