package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAGAMENTOS")
public class Pagamentos {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PAGAMENTOS", nullable = false)
    private Integer id;

    private TipoPagamento tipoPagamento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CLIENTES_PACOTES", nullable = false)
    private ClientesPacotes clientesPacotes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CONTRATACAO", nullable = false)
    private Contratacao contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }
}
