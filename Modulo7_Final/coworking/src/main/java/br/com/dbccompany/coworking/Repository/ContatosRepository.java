package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contatos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatosRepository extends CrudRepository<Contatos, Integer> {
    Optional<Contatos> findById (Integer id);
    List<Contatos> findAll();
}