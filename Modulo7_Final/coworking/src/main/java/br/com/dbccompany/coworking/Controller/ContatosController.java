package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contatos;
import br.com.dbccompany.coworking.Service.ContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contatos")
public class ContatosController {
    @Autowired
    private ContatosService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contatos adicionarContato(@RequestBody Contatos contatos ) {
        return service.salvar(contatos);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contatos editarContatos(@PathVariable Integer id, @RequestBody Contatos contatos) {
        return service.editar( id, java.util.Optional.ofNullable(contatos));
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarContato( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Contatos buscarContatoId(@PathVariable Integer id) {
        return service.contatoEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Contatos> todosContatos(){
        return service.todosContatos();
    }
}
