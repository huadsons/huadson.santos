package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Contratacao salvar(Contratacao contratacao ) {
        return repository.save( contratacao );
    }

    public List<Contratacao> todasContratacoes() {
        return repository.findAll();
    }

    public Contratacao contratacaoEspecifico( Integer id ){
        Optional<Contratacao> contratacao = repository.findById( id );
        return contratacao.get();
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
