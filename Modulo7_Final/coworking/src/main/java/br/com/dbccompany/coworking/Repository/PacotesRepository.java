package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public
interface PacotesRepository extends CrudRepository<Pacotes, Integer> {
    Optional<Pacotes> findById(Integer id);
    List<Pacotes> findAll();
}
