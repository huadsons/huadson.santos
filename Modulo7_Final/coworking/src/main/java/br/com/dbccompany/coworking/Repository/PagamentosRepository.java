package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface
PagamentosRepository extends CrudRepository<Pagamentos, Integer> {
    Optional<Pagamentos> findById (Integer id);
    List<Pagamentos> findAll();
}
