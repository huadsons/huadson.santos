package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "ACESSOS")
public class Acessos {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_ACESSOS", nullable = false)
    private Integer id;

    private Boolean isEntrada;

    private Date data;

    private Boolean isExcecao;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId( "ID_CLIENTES" )
    @JoinColumn(name = "ID_SALDO_CLIENTE", nullable = false)
    private Clientes clientes;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId( "ID_ESPACOS" )
    @JoinColumn(name = "ID_ESPACO_CLIENTE", nullable = false)
    private Espacos espacos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
