package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "PACOTES" )
public class Pacotes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PACOTES", nullable = false)
    private Integer id;

    private Double valor;

    @OneToMany(mappedBy = "pacotes")
    Set<EspacosPacotes> espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Set<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(Set<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }
}
