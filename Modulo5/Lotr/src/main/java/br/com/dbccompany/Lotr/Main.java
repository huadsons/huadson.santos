package br.com.dbccompany.Lotr;

import org.hibernate.Session;
import org.hibernate.Transaction;
import br.com.dbccompany.Lotr.Entity.HibernateUtil;
import br.com.dbccompany.Lotr.Entity.PersonagemEntity;

public class Main {
	

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			PersonagemEntity personagem = new PersonagemEntity();
			personagem.setNome("Qualquer");
			
			session.save(personagem);
			
			transaction.commit();
			
		}catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
		
	}
}
		
//		Connection conn = Connector.connect();
//		
//		try {
//			/*Vi se existe a tabela*/
//			ResultSet rs = conn.prepareStatement(" select tname from tab where tname = 'PAISES' ")
//					.executeQuery();
//			if (!rs.next()) {
//				
//				/*Se não tem cria a tabela*/
//				conn.prepareStatement(" CREATE TABLE PAISES(\n"
//						+ "ID_PAIS INTEGER PRIMARY KEY NOT NULL, \n"
//						+ "NOME VARCHAR(100) NOT NULL\n" 
//						+ ")").execute();
//			}
//			/*Insere dados na tabela*/
//			PreparedStatement pst = conn.prepareStatement("INSERT INTO PAISES ( ID_PAIS, NOME_PAISES )"
//					+ "VALUES ( SEQ_PAISES.NEXTVAL, ?)");
//			pst.setString(1, "BRASIL");
//			pst.executeUpdate();
//			
//			/*para visualizar a tabela*/
//			rs = conn.prepareStatement("SELECT * FROM PAISES").executeQuery();
//			
//			/*percorrer o array da tabela*/
//			while( rs.next() ) {
//				System.out.println(String.format("Nome do Pais: %s", rs.getString("NOME_PAISES")));
//			}
//			
//		}catch (SQLException ex) {
//			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA", ex);
//		}
	
