package br.com.dbccompany.Lotr.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

public class ItemEntity {
	
	@Id
	@GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_ITEM", nullable = false )
	private Integer id;
	private String descricao;
	
	@ManyToMany( mappedBy = "itens" )
	private List<InventarioXItem> InventarioXItem = new ArrayList<>();

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<InventarioXItem> getInventarioXItem() {
		return InventarioXItem;
	}
	
	public void setInventarioXItem(List<InventarioXItem> inventarioXItem) {
		InventarioXItem = inventarioXItem;
	}

}
