package br.com.dbccompany.Lotr.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


	@MappedSuperclass //vai ser criada dentro dos personagem, não vai ser uma tabela propria
	@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS)
	@Table(name = "PERSONAGEM")
	
	public class PersonagemEntity {
		
	@Id
	@SequenceGenerator( allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ" )
	
	@GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	private String nome;
	
	@Enumerated( EnumType.STRING )
	private StatusEntity status;
	
	private Double vida;
	
	@Column(name = "QTD_DANO")
	private Double qtddano;
	
	private Integer eperiencia;
	
	@Column( name = "QTD_EXPERIENCIA_POR_ATAQUE" )
	private Integer QtdExperienciaPorAtaque;
	
	@Enumerated( EnumType.STRING )
	private Tipo tipo;
	
	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_INVENTARIO" )
	private InventarioEntity inventario;

	public StatusEntity getStatus() {
		return status;
	}

	public void setStatus(StatusEntity status) {
		this.status = status;
	}

	public Double getVida() {
		return vida;
	}

	public void setVida(Double vida) {
		this.vida = vida;
	}

	public Double getQtddano() {
		return qtddano;
	}

	public void setQtddano(Double qtddano) {
		this.qtddano = qtddano;
	}

	public Integer getEperiencia() {
		return eperiencia;
	}

	public void setEperiencia(Integer eperiencia) {
		this.eperiencia = eperiencia;
	}

	public Integer getQtdExperienciaPorAtaque() {
		return QtdExperienciaPorAtaque;
	}

	public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
		QtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo( Tipo tipo) {
		this.tipo = tipo;
	}
	
	public Integer getId() {
 		return id;
 	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public InventarioEntity getInventario() {
		return inventario;
	}

	public void setInventario(InventarioEntity inventario) {
		this.inventario = inventario;
	}
	
	
}
