package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "ELFO_DA_LUZ" )

public class ElfoDaLuzEntity extends ElfoEntity{

	public ElfoDaLuzEntity() {
		super.setTipo( Tipo.ELFO_DA_LUZ );
	}
	
	@Column( name = "QTD_ATAQUE" )
	private Integer qtdAtaque;
	
	@Column( name = "QTD_VIDA_GANHA" )
	private Double qtdVidaGanha;
	
}
