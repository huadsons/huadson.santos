package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.ContasEntity;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    private ContaRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ContasEntity salvar(ContasEntity conta ){

        return repository.save( conta );
    }

    @Transactional( rollbackFor = Exception.class )
    public ContasEntity editar( ContasEntity conta, Integer id ){
        conta.setId(id);
        return repository.save( conta );
    }

    public List<ContasEntity> todasContas(){
        return (List<ContasEntity>) repository.findAll();
    }

    public ContasEntity contaEspecifico( Integer id ){
        Optional<ContasEntity> conta = repository.findById(id);
        return conta.get();
    }

}
