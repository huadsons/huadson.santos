package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class EstadosEntity {

    @Id
    @SequenceGenerator (allocationSize = 1, name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ" )
    @GeneratedValue (generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_ESTADOS")
    private Integer id;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PAISES" )
    private PaisesEntity paises;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CIDADES" )
    private CidadesEntity cidades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome_estados) {
        this.nome = nome;
    }

    public PaisesEntity getPaises() {
        return paises;
    }

    public void setPaises(PaisesEntity paises) {
        this.paises = paises;
    }
}
