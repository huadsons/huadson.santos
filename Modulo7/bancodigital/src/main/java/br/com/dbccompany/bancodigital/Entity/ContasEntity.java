package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class ContasEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTAS_SEQ", sequenceName = "CONTAS_SEQ" )
    @GeneratedValue( generator = "CONTAS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CONTAS")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_AGENCIAS" )
    private AgenciasEntity agencias;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_TIPO" )
    private TipoEntity tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AgenciasEntity getAgencias() {
        return agencias;
    }

    public void setAgencias(AgenciasEntity agencias) {
        this.agencias = agencias;
    }

    public TipoEntity getTipo() {
        return tipo;
    }

    public void setTipo(TipoEntity tipo) {
        this.tipo = tipo;
    }
}
