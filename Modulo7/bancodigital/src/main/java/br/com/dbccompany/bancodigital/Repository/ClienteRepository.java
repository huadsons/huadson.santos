package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClientesEntity, Integer> {
    ClientesEntity findByNome(String nome);
    List<ClientesEntity> findAll();

    Optional<ClientesEntity> findByCpf(Integer cpf);
}
