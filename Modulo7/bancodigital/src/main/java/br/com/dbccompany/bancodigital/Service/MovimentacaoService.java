package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.MovimentacaoEntity;
import br.com.dbccompany.bancodigital.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacaoService {

    @Autowired
    private MovimentacaoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public MovimentacaoEntity salvar(MovimentacaoEntity movimentacao ){

        return repository.save( movimentacao );
    }

    public List<MovimentacaoEntity> todasMovimentacoes(){

        return (List<MovimentacaoEntity>) repository.findAll();
    }

    public MovimentacaoEntity MovimentacaoEspecifica( Integer id ){
        Optional<MovimentacaoEntity> movimentacao = repository.findById(id);
        return movimentacao.get();
    }

}
