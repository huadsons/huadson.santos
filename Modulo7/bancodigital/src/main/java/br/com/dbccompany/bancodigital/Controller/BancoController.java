package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco" )
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<BancosEntity> todosBancos(){
        return service.todosBancos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public BancosEntity novoBanco(@RequestBody BancosEntity banco){
        return service.salvar(banco);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public BancosEntity editarBanco( @PathVariable Integer id, @RequestBody BancosEntity banco ){
        return service.editar( banco, id );
    }

}
