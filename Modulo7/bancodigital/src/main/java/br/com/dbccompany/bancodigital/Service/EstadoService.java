package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.EstadosEntity;
import br.com.dbccompany.bancodigital.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {

    @Autowired
    private EstadoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EstadosEntity salvar(EstadosEntity estado ){
        return repository.save( estado );
    }

    @Transactional( rollbackFor = Exception.class )
    public EstadosEntity editar( EstadosEntity estado, Integer id ){
        estado.setId(id);
        return repository.save( estado );
    }

    public List<EstadosEntity> todosEstados(){
        return (List<EstadosEntity>) repository.findAll();
    }

    public EstadosEntity estadoEspecifico( Integer id ){
        Optional<EstadosEntity> estado = repository.findById(id);
        return estado.get();
    }

}
