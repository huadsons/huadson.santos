package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.MovimentacaoEntity;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/movimentacao" )
public class MovimentacaoController {

    @Autowired
    MovimentacaoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<MovimentacaoEntity> todasMovimentacoes(){
        return service.todasMovimentacoes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public MovimentacaoEntity novaMovimentacao(@RequestBody MovimentacaoEntity movimentacao){
        return service.salvar(movimentacao);
    }


}
