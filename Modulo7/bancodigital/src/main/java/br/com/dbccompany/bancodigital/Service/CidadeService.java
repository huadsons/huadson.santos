package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.CidadesEntity;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public CidadesEntity salvar(CidadesEntity cidade ){
        return repository.save( cidade );
    }

    @Transactional( rollbackFor = Exception.class )
    public CidadesEntity editar( CidadesEntity cidade, Integer id ){
        cidade.setId(id);
        return repository.save( cidade );
    }

    public List<CidadesEntity> todasCidades(){
        return (List<CidadesEntity>) repository.findAll();
    }

    public CidadesEntity cidadeEspecifica( Integer id ){
        Optional<CidadesEntity> cidade = repository.findById(id);
        return cidade.get();
    }

}
