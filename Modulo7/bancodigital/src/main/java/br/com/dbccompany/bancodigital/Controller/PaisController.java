package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.PaisesEntity;
import br.com.dbccompany.bancodigital.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pais" )
public class PaisController {
    @Autowired
    PaisService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PaisesEntity> todosPaises(){
        return service.todosPaises();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PaisesEntity novoPais(@RequestBody PaisesEntity pais){
        return service.salvar(pais);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PaisesEntity editarPais( @PathVariable Integer id, @RequestBody PaisesEntity pais ){
        return service.editar( pais, id );
    }
}