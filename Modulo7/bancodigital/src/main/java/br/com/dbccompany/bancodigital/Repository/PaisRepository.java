package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.PaisesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends CrudRepository<PaisesEntity, Integer> {
    PaisesEntity findByNome(String nome);
    List<PaisesEntity> findAll();
}
