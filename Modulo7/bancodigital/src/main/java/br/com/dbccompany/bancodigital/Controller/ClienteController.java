package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.ClientesEntity;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cliente" )
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesEntity> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesEntity novoCliente(@RequestBody ClientesEntity cliente){
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesEntity editarCliente( @PathVariable Integer cpf, @RequestBody ClientesEntity cliente ){
        return service.editar( cliente, cpf );
    }

}
