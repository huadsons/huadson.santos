package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


public class ClientesXContas {

    @ManyToOne(cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTES" )
    @Column( name = "ID_CLIENTES" )
    private ClientesEntity clientes;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CONTAS" )
    @Column( name = "ID_CONTAS" )
    private ContasEntity contas;

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public ContasEntity getContas() {
        return contas;
    }

    public void setContas(ContasEntity contas) {
        this.contas = contas;
    }
}
