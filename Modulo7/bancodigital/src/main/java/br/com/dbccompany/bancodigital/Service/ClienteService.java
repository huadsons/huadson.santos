package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.ClientesEntity;
import br.com.dbccompany.bancodigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity salvar(ClientesEntity cliente ){
        return repository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity editar( ClientesEntity cliente, Integer id ){
        cliente.setId(id);
        return repository.save( cliente );
    }

    public List<ClientesEntity> todosClientes(){
        return (List<ClientesEntity>) repository.findAll();
    }

    public ClientesEntity clienteEspecifico( Integer cpf ){
        Optional<ClientesEntity> cliente = repository.findByCpf(cpf);
        return cliente.get();
    }

}
