package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class CidadesEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ" )
    @GeneratedValue( generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CIDADES")
    private Integer id;

    private String nome;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CIDADES_X_CLIENTES",
        joinColumns = { @JoinColumn ( name = "ID_CIDADES" )},
        inverseJoinColumns = { @JoinColumn( name = "ID_CLIENTES" )})

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESTADOS" )
    private EstadosEntity estados;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome_cidades) {
        this.nome = nome_cidades;
    }

    public EstadosEntity getEstados() {
        return estados;
    }

    public void setEstados(EstadosEntity estados) {
        this.estados = estados;
    }
}
