package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.CidadesEntity;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidade" )
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<CidadesEntity> todasCidades(){

        return service.todasCidades();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public CidadesEntity novaCidade(@RequestBody CidadesEntity cidade){

        return service.salvar(cidade);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public CidadesEntity editarCidade(@PathVariable Integer id, @RequestBody CidadesEntity cidade ){
        return service.editar( cidade, id );
    }

}
