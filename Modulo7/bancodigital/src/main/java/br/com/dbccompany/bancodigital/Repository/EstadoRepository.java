package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.EstadosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoRepository extends CrudRepository<EstadosEntity, Integer> {
    EstadosEntity findByNome(String nome);
    List<EstadosEntity> findAll();
}
