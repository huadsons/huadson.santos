package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class AgenciasEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ" )
    @GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name= "ID_AGENCIAS")
    private Integer id;

    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_CIDADE")
    private CidadesEntity cidade;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_BANCOS")
    private BancosEntity banco;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CidadesEntity getCidade() {
        return cidade;
    }

    public void setCidade(CidadesEntity cidade) {
        this.cidade = cidade;
    }

    public BancosEntity getBanco() {
        return banco;
    }

    public void setBanco(BancosEntity banco) {
        this.banco = banco;
    }
}
