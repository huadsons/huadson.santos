package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.AgenciasEntity;
import br.com.dbccompany.bancodigital.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgenciaService {

    @Autowired
    private AgenciaRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AgenciasEntity salvar( AgenciasEntity agencia ){
        return repository.save( agencia );
    }

    @Transactional( rollbackFor = Exception.class )
    public AgenciasEntity editar( AgenciasEntity agencia, Integer id ) {
        agencia.setId(id);
        return repository.save(agencia);
    }

    public List<AgenciasEntity> todasAgencias(){
        return repository.findAll();
    }

    public AgenciasEntity agenciaEspecifica( Integer id ){
        Optional<AgenciasEntity> agencia = repository.findById(id);
        return agencia.get();
    }

}