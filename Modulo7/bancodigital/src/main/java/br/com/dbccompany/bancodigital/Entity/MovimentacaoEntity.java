package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class MovimentacaoEntity {

    @Id
    @SequenceGenerator (allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ" )
    @GeneratedValue (generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_MOVIMENTACAO")
    private Integer id;
    private String movimentacao;

    @ManyToOne(cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CONTAS" )
    private ContasEntity contas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMovimentacao() {
        return movimentacao;
    }

    public void setMovimentacao(String movimentacao) {
        this.movimentacao = movimentacao;
    }

    public ContasEntity getContas() {
        return contas;
    }

    public void setContas(ContasEntity contas) {
        this.contas = contas;
    }
}
