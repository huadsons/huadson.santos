package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.ContasEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<ContasEntity, Integer> {
    ContasEntity findByTipo(Integer tipo);
    List<ContasEntity> findAll();
}
