package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.AgenciasEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgenciaRepository extends CrudRepository<AgenciasEntity, Integer>{
    AgenciasEntity findByNome (String nome);
    List<AgenciasEntity> findAll();
}