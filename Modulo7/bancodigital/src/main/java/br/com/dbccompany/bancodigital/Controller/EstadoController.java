package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.EstadosEntity;
import br.com.dbccompany.bancodigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/estado" )
public class EstadoController {

    @Autowired
    EstadoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EstadosEntity> todosEstados(){
        return service.todosEstados();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EstadosEntity novoEstado(@RequestBody EstadosEntity estado){
        return service.salvar(estado);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EstadosEntity editarEstado( @PathVariable Integer id, @RequestBody EstadosEntity estado ){
        return service.editar( estado, id );
    }

}
