package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.ContasEntity;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/conta" )
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContasEntity> todasContas(){

        return service.todasContas();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContasEntity novaConta(@RequestBody ContasEntity conta){

        return service.salvar(conta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ContasEntity editarConta( @PathVariable Integer id, @RequestBody ContasEntity conta ){
        return service.editar( conta, id );
    }

}
