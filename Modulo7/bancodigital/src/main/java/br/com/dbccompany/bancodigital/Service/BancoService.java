package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public BancosEntity salvar( BancosEntity banco ){
        return repository.save( banco );
    }

    @Transactional( rollbackFor = Exception.class )
    public BancosEntity editar( BancosEntity banco, Integer id ){
        banco.setId(id);
        return repository.save( banco );
    }

    public List<BancosEntity> todosBancos(){
        return (List<BancosEntity>) repository.findAll();
    }

    public BancosEntity bancoEspecifico( Integer id ){
        Optional<BancosEntity> banco = repository.findById(id);
        return banco.get();
    }

}
