package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import br.com.dbccompany.bancodigital.Entity.CidadesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadeRepository extends CrudRepository<CidadesEntity, Integer> {
    CidadesEntity findByNome(String nome);
    List<CidadesEntity> findAll();
}
