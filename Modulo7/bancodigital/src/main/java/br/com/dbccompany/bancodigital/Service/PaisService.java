package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.PaisesEntity;
import br.com.dbccompany.bancodigital.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PaisesEntity salvar(PaisesEntity pais ){
        return repository.save( pais );
    }

    @Transactional( rollbackFor = Exception.class )
    public PaisesEntity editar( PaisesEntity pais, Integer id ){
        pais.setId(id);
        return repository.save( pais );
    }

    public List<PaisesEntity> todosPaises(){

        return (List<PaisesEntity>) repository.findAll();
    }

    public PaisesEntity paisEspecifico( Integer id ){
        Optional<PaisesEntity> pais = repository.findById(id);
        return pais.get();
    }

}
