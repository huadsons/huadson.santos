package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class ClientesEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_CLIENTES")
    private Integer id;
    private String nome;
    private Integer cpf;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCpf() {
        return cpf;
    }

    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }
}
