package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class TipoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "TIPO_SEQ", sequenceName = "TIPO_SEQ" )
    @GeneratedValue( generator = "TIPO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name= "ID_TIPOS")
    private Integer id;
    private Integer tipo;

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
}
