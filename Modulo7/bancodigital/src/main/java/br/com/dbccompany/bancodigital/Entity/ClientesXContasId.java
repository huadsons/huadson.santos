package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Embeddable
public class ClientesXContasId {

    @Column(name = "ID_CLIENTES")
    private Integer idClientes;
    @Column (name = "ID_CONTAS")
    private Integer idContas;
    public ClientesXContasId() {}
    public ClientesXContasId(Integer idClientes, Integer idContas){
        this.idClientes = idClientes;
        this.idContas = idContas;
    }

    public Integer getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Integer idClientes) {

        this.idClientes = idClientes;
    }

    public Integer getIdContas() {

        return idContas;
    }

    public void setIdContas(Integer idContas) {

        this.idContas = idContas;
    }

}
