package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.BancosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<BancosEntity, Integer> {
    BancosEntity findByNome (String nome);
    List<BancosEntity> findAll();
}
