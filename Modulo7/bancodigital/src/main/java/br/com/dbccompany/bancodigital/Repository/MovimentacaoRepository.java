package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.MovimentacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovimentacaoRepository extends CrudRepository<MovimentacaoEntity, Integer> {
    Optional<MovimentacaoEntity> findById(Integer id);
    List<MovimentacaoEntity> findAll();
}
