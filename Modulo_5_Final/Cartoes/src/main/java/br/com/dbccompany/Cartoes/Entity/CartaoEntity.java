package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

public class CartaoEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ" )
	@GeneratedValue( generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_CARTAO" )
	private Integer id;
	
	private String chip_cartao;
	
	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_CLIENTE" )
	private LancamentoEntity cliente;
	
	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_BANDEIRA" )
	private LancamentoEntity bandeira;
	
	@OneToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_EMISSOR" )
	private LancamentoEntity emissor;
	
	private Date vencimento_cartao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChip_cartao() {
		return chip_cartao;
	}

	public void setChip_cartao(String chip_cartao) {
		this.chip_cartao = chip_cartao;
	}

	public LancamentoEntity getCliente() {
		return cliente;
	}

	public void setCliente(LancamentoEntity cliente) {
		this.cliente = cliente;
	}

	public LancamentoEntity getBandeira() {
		return bandeira;
	}

	public void setBandeira(LancamentoEntity bandeira) {
		this.bandeira = bandeira;
	}

	public LancamentoEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(LancamentoEntity emissor) {
		this.emissor = emissor;
	}

	public Date getVencimento_cartao() {
		return vencimento_cartao;
	}

	public void setVencimento_cartao(Date vencimento_cartao) {
		this.vencimento_cartao = vencimento_cartao;
	}
	
	

}
