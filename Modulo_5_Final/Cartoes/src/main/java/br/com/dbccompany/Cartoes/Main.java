package br.com.dbccompany.Cartoes;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;

public class Main {
	

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			
			loja.setNome_loja("Qualquer");
			
			session.save(loja);
			
			transaction.commit();
			
		}catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
		
	}
}