package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

public class CredenciadorEntity {
	
	@Id
	@GeneratedValue( generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_CREDENCIADOR", nullable = false )
	private Integer id;
	
	@Column( name = "NOME_CREDENCIADOR", nullable = false )
	private String nome_credenciador;
	
	@ManyToMany( mappedBy = "credenciador" )
	private List<LojaXCredenciador> LojaXCredenciador = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome_credenciador() {
		return nome_credenciador;
	}

	public void setNome_credenciador(String nome_credenciador) {
		this.nome_credenciador = nome_credenciador;
	}

	public List<LojaXCredenciador> getLojaXCredenciador() {
		return LojaXCredenciador;
	}

	public void setLojaXCredenciador(List<LojaXCredenciador> lojaXCredenciador) {
		LojaXCredenciador = lojaXCredenciador;
	}

	
}
