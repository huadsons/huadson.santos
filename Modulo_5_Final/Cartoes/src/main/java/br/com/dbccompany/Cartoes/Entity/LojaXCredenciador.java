package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.util.List;


@Entity
@Table( name = "LOJA_X_CREDENCIADOR" )
@SequenceGenerator( allocationSize = 1, name = "LOJA_X_CREDENCIADOR", sequenceName = "LOJA_X_CREDENCIADOR" )
public class LojaXCredenciador {
	
	@Id
	@GeneratedValue( generator = "LOJA_X_CREDENCIADOR", strategy = GenerationType.SEQUENCE )
	@Column( name = "LOJA_X_CREDENCIADOR" )
	private Integer id;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "id_loja",
		joinColumns = {
				@JoinColumn( name = "id_loja_x_credenciador" )},
		inverseJoinColumns = {
				@JoinColumn( name = "id_loja" )})
	private List<LojaEntity> loja = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "id_credenciador",
		joinColumns = {
				@JoinColumn( name = "id_loja_x_credenciador" )},
		inverseJoinColumns = {
				@JoinColumn( name = "id_credenciador" )})
	private List<LojaEntity> credenciador = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<LojaEntity> getLoja() {
		return loja;
	}

	public void setLoja(List<LojaEntity> loja) {
		this.loja = loja;
	}

	public List<LojaEntity> getCredenciador() {
		return credenciador;
	}

	public void setCredenciador(List<LojaEntity> credenciador) {
		this.credenciador = credenciador;
	}
	
	
	
}
	