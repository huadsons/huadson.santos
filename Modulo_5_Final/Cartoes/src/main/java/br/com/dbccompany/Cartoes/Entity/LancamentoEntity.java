package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.crypto.Data;

@Entity
@Table(name = "LANCAMENTO")

public class LancamentoEntity {
	
@Id
@SequenceGenerator( allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName = "LANCAMENTO_SEQ" )
@GeneratedValue( generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
@Column( name = "ID_LANCAMENTO" )
private Integer id;

@OneToOne( cascade = CascadeType.ALL )
@JoinColumn( name = "FK_ID_CARTAO" )
private LancamentoEntity cartao;

@OneToOne( cascade = CascadeType.ALL )
@JoinColumn( name = "FK_ID_LOJA" )
private LancamentoEntity loja;

@OneToOne( cascade = CascadeType.ALL )
@JoinColumn( name = "FK_ID_EMISSOR" )
private LancamentoEntity emissor;


private String descricao_lancamento;

private Double valor_lancamento;

private Date data_compra;

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public LancamentoEntity getCartao() {
	return cartao;
}

public void setCartao(LancamentoEntity cartao) {
	this.cartao = cartao;
}

public LancamentoEntity getLoja() {
	return loja;
}

public void setLoja(LancamentoEntity loja) {
	this.loja = loja;
}

public LancamentoEntity getEmissor() {
	return emissor;
}

public void setEmissor(LancamentoEntity emissor) {
	this.emissor = emissor;
}

public String getDescricao_lancamento() {
	return descricao_lancamento;
}

public void setDescricao_lancamento(String descricao_lancamento) {
	this.descricao_lancamento = descricao_lancamento;
}

public Double getValor_lancamento() {
	return valor_lancamento;
}

public void setValor_lancamento(Double valor_lancamento) {
	this.valor_lancamento = valor_lancamento;
}

public Date getData_compra() {
	return data_compra;
}

public void setData_compra(Date data_compra) {
	this.data_compra = data_compra;
}



}
