package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

public class BandeiraEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "BANDEIRA_SEQ", sequenceName = "BANDEIRA_SEQ" )
	@GeneratedValue( generator = "BANDEIRA_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_BANDEIRA" )
	private Integer id;

	private String nome_bandeira;
	
	private Double taxa_bandeira;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome_bandeira() {
		return nome_bandeira;
	}

	public void setNome_bandeira(String nome_bandeira) {
		this.nome_bandeira = nome_bandeira;
	}

	public Double getTaxa_bandeira() {
		return taxa_bandeira;
	}

	public void setTaxa_bandeira(Double taxa_bandeira) {
		this.taxa_bandeira = taxa_bandeira;
	}
	
	
}
