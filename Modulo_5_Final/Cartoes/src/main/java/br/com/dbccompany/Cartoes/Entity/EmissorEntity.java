package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EMISSOR")

public class EmissorEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "EMISSOR_SEQ", sequenceName = "EMISSOR_SEQ" )
	@GeneratedValue( generator = "EMISSOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_EMISSOR" )
	private Integer id;

	private String nome_emissor;
	
	private Double taxa_emissor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome_emissor() {
		return nome_emissor;
	}

	public void setNome_emissor(String nome_emissor) {
		this.nome_emissor = nome_emissor;
	}

	public Double getTaxa_emissor() {
		return taxa_emissor;
	}

	public void setTaxa_emissor(Double taxa_emissor) {
		this.taxa_emissor = taxa_emissor;
	}
}
